#primer paso
from airflow import DAG, utils
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.models import Variable
from random import randint
import pendulum

start = utils.dates.days_ago(3).astimezone(pendulum.timezone(Variable.get('TZ')))

def _choose_best_model(**kwargs):
    accuracies = kwargs['ti'].xcom_pull(task_ids=[
        'training_model_A',
        'training_model_B',
        'training_model_C'
    ])
    best_accuracy = max(accuracies)
    if (best_accuracy > 8):
        return 'accurate'
    return 'inaccurate'


def _training_model():
    return randint(1, 10)
  
#segundo paso
with DAG("DO_004_Modelo_Xcom3", start_date=start, schedule_interval="00 17 * * *", catchup=True) as dag:
#tercer paso
    t0 = DummyOperator(
    task_id='Inicio_17_00'
        ) 

    training_model_A = PythonOperator(
        task_id="training_model_A",
        python_callable=_training_model
        )

    training_model_B = PythonOperator(
        task_id="training_model_B",
        python_callable=_training_model
        )

    training_model_C = PythonOperator(
        task_id="training_model_C",
        python_callable=_training_model
        )

    choose_best_model = BranchPythonOperator(
        task_id="choose_best_model",
        provide_context=True,
        python_callable=_choose_best_model
        )

    accurate = BashOperator(
        task_id="accurate",
        bash_command="echo 'accurate'"
        )

    inaccurate = BashOperator(
        task_id="inaccurate",
        bash_command="echo 'inaccurate'"
        )

#cuarto paso
t0 >> [training_model_A, training_model_B, training_model_C] >> choose_best_model >> [accurate, inaccurate]

