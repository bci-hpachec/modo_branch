/*********************************************************************
**********************************************************************
** DSCRPCN: SE GENERAN LAS FECHAS DEL PROCESO                  	  **
**			               EL PUBLICO OBJETIVO   							        **
**          			 											**
** AUTOR  : BRUNO GOMEZ				                                        **
** EMPRESA: BCI                                 **
** FECHA  : 03/2020                                                 **
*********************************************************************/
/*********************************************************************
** MANTNCN: BRUNO GOMEZ                                                         **
** AUTOR  : BRUNO GOMEZ                                                         **
** FECHA  : 03/2020                                                 **
*********************************************************************
** TABLA DE ENTRADA : {{ var.value.BD_ANALYTICS}}.S_PERSONA_HIS_DIA    **
** TABLA DE SALIDA  : edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec  **
**                    edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct		**
**********************************************************************
*********************************************************************/
.SET SESSION CHARSET 'UTF8';
SELECT DATE, TIME;

/* **********************************************************************/
/* 			COMENTARIOS       */
/* **********************************************************************/
-- El proceso siempre esta con la fecha mas actual, es decir, si hoy es 202011
-- la informacion que debe contener es la del 202011 (esto es independiente de
-- si la informacion tiene desfase, ejemplo: info gestion o potencial)

/* **********************************************************************/
/* 			SE CREA LA TABLA QUE CONTIENE LA FECHA PARAMETRICA       */
/* **********************************************************************/
DROP TABLE edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec;
CREATE TABLE edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec
     (
      pf_fecha DATE FORMAT 'yyyy-mm-dd',
      pf_fecha_Ini DATE FORMAT 'yyyy-mm-dd',
      pc_fecha_ref CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
      pf_fecha_M1 DATE FORMAT 'yyyy-mm-dd',
      pc_fecha_ref_M1 CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
      pf_fecha_M2 DATE FORMAT 'yyyy-mm-dd',
      pc_fecha_ref_M2 CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
      pf_fecha_M6 DATE FORMAT 'yyyy-mm-dd',
      pc_fecha_ref_M6 CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC)
PRIMARY INDEX ( pf_fecha );
.IF ERRORCODE <> 0 THEN .QUIT 0001;

/* **********************************************************************/
/* 			SE INSERTA LA TABLA QUE CONTIENE LA FECHA PARAMETRICA       */
/* **********************************************************************/
INSERT INTO edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec
SELECT
       CAST('{{ next_ds }}' AS DATE)   AS pf_fecha, -- CAST('20200904' AS DATE FORMAT 'YYYYMMDD')
       ADD_MONTHS((pf_fecha - EXTRACT(DAY FROM pf_fecha)+1), 0) AS pf_fecha_Ini,
       CAST(pf_fecha_Ini AS DATE FORMAT 'YYYYMMDD')(CHAR(6)) AS pc_fecha_ref,
       ADD_MONTHS(pf_fecha_Ini, -1) AS pf_fecha_M1,
       CAST( pf_fecha_M1 AS DATE FORMAT 'YYYYMMDD')(CHAR(6)) AS pc_fecha_ref_M1,
       ADD_MONTHS(pf_fecha_Ini, -2) AS pf_fecha_M2,
       CAST( pf_fecha_M2 AS DATE FORMAT 'YYYYMMDD')(CHAR(6)) AS pc_fecha_ref_M2,
       ADD_MONTHS( pf_fecha_Ini , -6) AS pf_fecha_M6,
       CAST(pf_fecha_M6 AS DATE FORMAT 'YYYYMMDD')(CHAR(6)) AS pc_fecha_ref_M6
;
.IF ERRORCODE <> 0 THEN .QUIT 0002;
COLLECT STATS INDEX (pf_fecha)
, COLUMN pc_fecha_ref_M1
ON edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec;
.IF ERRORCODE <> 0 THEN .QUIT 0003;
/* **********************************************************************/
/* 			SE INSERTA LA TABLA QUE CONTIENE LA FECHA PARAMETRICA       */
/* **********************************************************************/
DROP TABLE edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct;
CREATE TABLE edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct
     (
      pc_fecha_ref CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
      pe_party_id INTEGER,
      pe_rut INTEGER,
      pe_est_civil CHAR(3) CHARACTER SET LATIN NOT CASESPECIFIC,
      pe_edad INTEGER)
PRIMARY INDEX ( pc_fecha_ref ,pe_party_id ,pe_rut );
.IF ERRORCODE <> 0 THEN .QUIT 0004;

INSERT INTO edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct
SELECT
  A.pc_fecha_ref
	, B.se_per_party_id
	, B.se_per_rut
	, B.sc_per_est_civil
  , B.se_per_edad
FROM edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec A
LEFT JOIN {{ var.value.BD_ANALYTICS}}.S_PERSONA_HIS_DIA B
	ON LAST_DAY(b.sf_per_fecha_info) = LAST_DAY(a.pf_fecha - INTERVAL '1' MONTH)
  AND b.sc_per_escliente = 'S'
  AND b.se_per_ind_cct = 1
  AND b.sc_per_banca IN ('PP', 'PRE', 'PBP', 'PBU','PBM')
  AND b.se_per_rut <= 50000000 -- modificacion v3.1 - habian ruts sobre 50MM aun usando los filtros anteriores
;
.IF ERRORCODE <> 0 THEN .QUIT 0005;


---- FIN TABLON
SELECT DATE, TIME;
.QUIT 0;
