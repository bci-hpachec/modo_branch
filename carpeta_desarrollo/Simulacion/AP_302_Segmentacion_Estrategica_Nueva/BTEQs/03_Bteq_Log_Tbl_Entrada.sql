/*********************************************************************
**********************************************************************
** DSCRPCN: GENERAMOS LOG PARA REVISAR REGISTROS DE LAS TABLAS DE ENTRADA**
**          			 											**
** AUTOR  : BRUNO GOMEZ				                                        **
** EMPRESA: BCI                                 **
** FECHA  : 06/2021                                                 **
*********************************************************************/
/*********************************************************************
** MANTNCN: BRUNO GOMEZ                                                   **
** AUTOR  : BRUNO GOMEZ                                                   **
** FECHA  : 05/2021                                                **
*********************************************************************
** TABLA DE ENTRADA : {{ var.value.BD_ANALYTICS}}.MP_INV_PROB_HIST               **
**                    {{ var.value.BD_ANALYTICS}}.AP_RENTABILIDAD_MERCADO_CLI           **
**                    {{ var.value.BD_ANALYTICS}}.MP_CLIRTAESTBCI_HIST           **
**                    {{ var.value.BD_ANALYTICS}}.AP_POTENCIAL_CICLO_CLI_HIST **
** TABLA DE SALIDA  : {{ var.value.BD_ANALYTICS}}.R_AP_SEG_EST_CLI_BCI_HIST              **
**********************************************************************
*********************************************************************/
.set session charset 'utf8';
select date, time;

/* **********************************************************************/
/* 			CREAMOS TABLA CON FECHAS       */
/* **********************************************************************/
-- Esto es para verificar que las tablas de entrada en la segmentacion tengan datos
DROP TABLE edw_tempusu.T_test_data_entrada;
CREATE SET TABLE edw_tempusu.T_test_data_entrada
     (
      tf_fecha DATE FORMAT 'YY/MM/DD',
      tf_fecha_m1 DATE FORMAT 'YY/MM/DD',
      tf_fecha_m2 DATE FORMAT 'YY/MM/DD',
      tc_fecha_ref CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
      tc_fecha_m1_ref CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
      tc_fecha_m2_ref CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC)
PRIMARY INDEX ( tf_fecha );
.if errorcode <> 0 then .quit 1;

INSERT INTO edw_tempusu.T_test_data_entrada
  SELECT CAST('{{ next_ds }}' AS DATE) AS tf_fecha -- Current_date se puede reemplazar por la fecha que corresponde
  , add_months(tf_fecha,-1) as tf_fecha_m1
  , add_months(tf_fecha,-2) as tf_fecha_m2
  , cast(tf_fecha as date format 'yyyymmdd')(char(6)) as tc_fecha_ref
  , cast(tf_fecha_m1 as date format 'yyyymmdd')(char(6)) as tc_fecha_m1_ref
  , cast(tf_fecha_m2 as date format 'yyyymmdd')(char(6)) as tc_fecha_m2_ref
;
.if errorcode <> 0 then .quit 2;


/* **********************************************************************/
/* 			REVISION DE TABLAS DE ENTRADA        */
/* **********************************************************************/
-- Pendiente: incorporar revision de tabla de gestion (aunque pesa bastante)
DEL FROM {{ var.value.BD_ANALYTICS}}.R_AP_SEG_EST_CLI_BCI_HIST A,
    edw_tempusu.T_test_data_entrada B
  WHERE  A.rc_fecha_ref  = B.tc_fecha_ref ;
  .if errorcode <> 0 then .quit 3;

INSERT INTO {{ var.value.BD_ANALYTICS}}.R_AP_SEG_EST_CLI_BCI_HIST
	SELECT
  B.tc_fecha_ref
	, '{{ var.value.BD_ANALYTICS}}.MP_INV_PROB_HIST' AS tabla
	, count(*) AS cantidad
	, SUM(CASE WHEN A.valor IS NULL THEN 1 ELSE 0 END) AS n_nulls_prob
	FROM {{ var.value.BD_ANALYTICS}}.MP_INV_PROB_HIST A -- count y null sobre m1
	INNER JOIN edw_tempusu.T_test_data_entrada B
		ON A.fecha_ref = B.tc_fecha_m1_ref
  GROUP BY B.tc_fecha_ref
;
.if errorcode <> 0 then .quit 4;


INSERT INTO {{ var.value.BD_ANALYTICS}}.R_AP_SEG_EST_CLI_BCI_HIST
	SELECT
  B.tc_fecha_ref
	, '{{ var.value.BD_ANALYTICS}}.AP_RENTABILIDAD_MERCADO_CLI' AS tabla
	, count(*) AS cantidad
	, SUM(CASE WHEN A.rentabilidad_mercado IS NULL THEN 1 ELSE 0 END) AS n_nulls_prob
	FROM {{ var.value.BD_ANALYTICS}}.AP_RENTABILIDAD_MERCADO_CLI A -- count y null sobre m1
	INNER JOIN edw_tempusu.T_test_data_entrada B
		ON A.fecha_ref = B.tc_fecha_m1_ref
  GROUP BY B.tc_fecha_ref
;
.if errorcode <> 0 then .quit 5;


INSERT INTO {{ var.value.BD_ANALYTICS}}.R_AP_SEG_EST_CLI_BCI_HIST
	SELECT
  B.tc_fecha_ref
	, '{{ var.value.BD_ANALYTICS}}.AP_POTENCIAL_CICLO_CLI_HIST' AS tabla
	, count(*) AS cantidad
	, SUM(CASE WHEN A.ie_potencial_ciclo_cli IS NULL THEN 1 ELSE 0 END) AS n_nulls_prob
	FROM {{ var.value.BD_ANALYTICS}}.AP_POTENCIAL_CICLO_CLI_HIST A
	INNER JOIN edw_tempusu.T_test_data_entrada B
		ON A.ic_fecha_ref = B.tc_fecha_m2_ref
  GROUP BY B.tc_fecha_ref
;
.if errorcode <> 0 then .quit 6;


INSERT INTO {{ var.value.BD_ANALYTICS}}.R_AP_SEG_EST_CLI_BCI_HIST
	SELECT
  B.tc_fecha_ref
	, '{{ var.value.BD_ANALYTICS}}.MP_CliRtaEstBCI_HIST' AS tabla
	, count(*) AS cantidad
	, SUM(CASE WHEN A.rta_estbci_final IS NULL THEN 1 ELSE 0 END) AS n_nulls_prob
	FROM {{ var.value.BD_ANALYTICS}}.MP_CliRtaEstBCI_HIST A -- count y null sobre m1
	INNER JOIN edw_tempusu.T_test_data_entrada B
		ON A.fecha_ref = B.tc_fecha_m1_ref
  GROUP BY B.tc_fecha_ref
;
.if errorcode <> 0 then .quit 7;

INSERT INTO {{ var.value.BD_ANALYTICS}}.R_AP_SEG_EST_CLI_BCI_HIST
	SELECT
  B.tc_fecha_ref
	, '{{ var.value.BD_ANALYTICS}}.I_riesgo1_hist' AS tabla
	, count(*) AS cantidad
	, SUM(CASE WHEN A.ic_Rango_PI IS NULL THEN 1 ELSE 0 END) AS n_nulls_prob
	FROM {{ var.value.BD_ANALYTICS}}.I_riesgo1_hist A -- count y null sobre fecha actual
	INNER JOIN edw_tempusu.T_test_data_entrada B
		ON A.ic_fecha_ref = B.tc_fecha_ref -- esto es porque tc_fecha_ref es el dato mas reciente
  GROUP BY B.tc_fecha_ref
;
.if errorcode <> 0 then .quit 8;

/* **********************************************************************/
/* 			ELIMINAMOS TABLA DE ENTRADA       */
/* **********************************************************************/

DROP TABLE edw_tempusu.T_test_data_entrada;

select date, time;
.quit 0;
