/*********************************************************************
**********************************************************************
** DSCRPCN: SE GENERA EL PERFILAMIENTO Y SEGMENTACION DE LA	**
**			CARTERA DE CCT DE BCI							**
**          			 											**
** AUTOR  : BRUNO GOMEZ				                                        **
** EMPRESA: BCI                                 **
** FECHA  : 03/2020                                                 **
*********************************************************************/
/*********************************************************************
** MANTNCN: BRUNO GOMEZ                                                         **
** AUTOR  : BRUNO GOMEZ                                                         **
** FECHA  : 05/2021                                                **
*********************************************************************
** TABLA DE ENTRADA : edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec  **
**                    edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct		**
**                    EDW_DMANALIC_VW.PBD_CONTRATOS			        **
**                    {{ var.value.BD_ANALYTICS}}.MP_INV_PROB_HIST                **
**                    {{ var.value.BD_ANALYTICS}}.AP_RENTABILIDAD_MERCADO_CLI            **
**                    {{ var.value.BD_ANALYTICS}}.MP_CLIRTAESTBCI_HIST           **
**                    {{ var.value.BD_ANALYTICS}}.C_CORTES_NEG_SEG_EST_PER           **
**                    EDW_VW.BCI_RCP_GST                                 **
**                    {{ var.value.BD_ANALYTICS}}.AP_POTENCIAL_CICLO_CLI_HIST **
**                    {{ var.value.BD_ANALYTICS}}.I_riesgo1_hist   				     **
** TABLA DE SALIDA  : {{ var.value.BD_ANALYTICS}}.I_AP_SEG_EST_CLI_BCI  **
**                    {{ var.value.BD_ANALYTICS}}.I_AP_SEG_EST_CLI_BCI_HIST		**
**********************************************************************
*********************************************************************/
.SET SESSION CHARSET 'UTF8';
SELECT DATE, TIME;

/* **********************************************************************/
/* 			SE CREA TABLA RECUPERANDO LOS AUM DE LOS CLIENTES     */
/* **********************************************************************/
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_AUM_ESTIMADO;
CREATE MULTISET TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_AUM_ESTIMADO
     (
      Te_rut INTEGER,
      Te_fecha_ref INTEGER,
      Te_MODELO_ID INTEGER,
      Td_valor DECIMAL(18,4))
PRIMARY INDEX ( Te_rut );
.IF ERRORCODE <> 0 THEN .QUIT 0001;

/* **********************************************************************/
/* 			SE INSERTA TABLA RECUPERANDO LOS AUM DE LOS CLIENTES     */
/* **********************************************************************/
INSERT INTO EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_AUM_ESTIMADO
	SELECT A.Rut
  , A.fecha_ref
  , A.modelo_id
  , A.valor
  FROM {{ var.value.BD_ANALYTICS}}.MP_INV_PROB_HIST A
	INNER JOIN  edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec B ON A.fecha_ref = B.pc_fecha_ref_m1
	INNER JOIN edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct C ON A.rut = C.pe_rut
	WHERE A.MODELO_ID = 11;
.IF ERRORCODE <> 0 THEN .QUIT 0002;

COLLECT STATS INDEX (Te_rut)
ON EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_AUM_ESTIMADO;
.IF ERRORCODE <> 0 THEN .QUIT 0003;

/* **********************************************************************/
/* 			SE CREA TABLA DE RIESGO 1 HISTORICA PARA EL MES DE ESTUDIO     */
/* **********************************************************************/
-- modificacion hecha el 202109 - v3.2
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_riesgo1;
CREATE MULTISET TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_riesgo1
     (
      tc_fecha_ref VARCHAR(6) CHARACTER SET UNICODE NOT CASESPECIFIC,
      te_rut BIGINT,
      tc_ESTRATEGIA VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
      tc_Rango_PI VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
      tc_PasaFiltrosDuros VARCHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      tc_PasaFiltrosMenosSeveros VARCHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC)
PRIMARY INDEX ( tc_fecha_ref ,te_rut );
.IF ERRORCODE <> 0 THEN .QUIT 0004;

/* **********************************************************************/
/* 			SE INSERTA TABLA RECUPERANDO LOS AUM DE LOS CLIENTES     */
/* **********************************************************************/
-- modificacion hecha el 202109 - v3.2
INSERT INTO EDW_TEMPUSU.T_{{next_ds_nodash}}_riesgo1
	SELECT A.ic_fecha_ref
  , A.ie_rut
  , A.ic_ESTRATEGIA
  , A.ic_Rango_PI
  , A.ic_PasaFiltrosDuros
  , A.ic_PasaFiltrosMenosSeveros
  FROM {{ var.value.BD_ANALYTICS}}.I_riesgo1_hist A
	INNER JOIN  edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec B ON A.ic_fecha_ref = B.pc_fecha_ref
	INNER JOIN edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct C ON A.ie_rut = C.pe_rut
;
.IF ERRORCODE <> 0 THEN .QUIT 0005;

COLLECT STATS INDEX (tc_fecha_ref ,te_rut)
ON EDW_TEMPUSU.T_{{next_ds_nodash}}_riesgo1;
.IF ERRORCODE <> 0 THEN .QUIT 0006;

/* **********************************************************************/
/* 			SE CREA TABLA RECUPERANDO EL INR SISTEMA DE LOS CLIENTES     */
/* **********************************************************************/
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_INR_SISTEMA ;
CREATE MULTISET TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_INR_SISTEMA
     (
      Te_rut INTEGER,
      Te_fecha_ref_inr_sist INTEGER,
      td_avg6m_inr_bci FLOAT,
      Td_INR_sistema FLOAT,
      Td_vinculacion_inr FLOAT)
PRIMARY INDEX ( Te_rut, Te_fecha_ref_inr_sist );
.IF ERRORCODE <> 0 THEN .QUIT 0007;

/* **********************************************************************/
/* 			SE INSERTA TABLA RECUPERANDO EL INR SISTEMA DE LOS CLIENTES     */
/* **********************************************************************/
INSERT INTO EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_INR_SISTEMA
	SELECT A.rut AS Te_rut
  , A.FECHA_REF AS Te_fecha_ref_inr_sist
  , A.avg6m_inr_bci as avg6m_inr_bci -- en realidad es margen_financiero + Gasto_Riesgo
  , A.rentabilidad_mercado AS Td_INR_sistema
  , A.vinculacion -- la vinculacion igual
	FROM {{ var.value.BD_ANALYTICS}}.AP_RENTABILIDAD_MERCADO_CLI A
	INNER JOIN  edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec B ON A.fecha_ref = B.pc_fecha_ref_M1 -- desfase de dos meses INR mercado
	INNER JOIN edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct C ON A.rut = C.pe_rut
;
.IF ERRORCODE <> 0 THEN .QUIT 0008;

COLLECT STATS INDEX (Te_rut, Te_fecha_ref_inr_sist)
ON EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_INR_SISTEMA;
.IF ERRORCODE <> 0 THEN .QUIT 0009;

/* **********************************************************************/
/* 			SE CREA TABLA RECUPERANDO EL POTENCIAL DE CICLO DE LOS CLIENTES     */
/* **********************************************************************/
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_INR_POTENCIAL_CICLO ;
CREATE MULTISET TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_INR_POTENCIAL_CICLO
     (
      Te_rut INTEGER,
      Te_FECHA_REF INTEGER,
      te_potencial_ciclo_cli FLOAT,
      tc_grupo_potencial_ciclo VARCHAR(15) CHARACTER SET LATIN NOT CASESPECIFIC)
PRIMARY INDEX ( Te_rut );
.IF ERRORCODE <> 0 THEN .QUIT 0010;

/* **********************************************************************/
/* 			SE INSERTA TABLA RECUPERANDO EL POTENCIAL DE CICLO DE LOS CLIENTES     */
/* **********************************************************************/
INSERT INTO EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_INR_POTENCIAL_CICLO
	SELECT A.ie_rut as te_rut
  , A.ic_fecha_ref as tc_fecha_ref
  , A.ie_potencial_ciclo_cli as te_potencial_ciclo_cli
  , A.ic_grupo_potencial_ciclo as tc_grupo_potencial_ciclo
	FROM {{ var.value.BD_ANALYTICS}}.AP_POTENCIAL_CICLO_CLI_HIST A
	INNER JOIN  edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec B ON A.ic_fecha_ref = B.pc_fecha_ref_M2 -- desfase de dos meses potencial de ciclo
	INNER JOIN edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct C ON A.ie_rut = C.pe_rut
;
.IF ERRORCODE <> 0 THEN .QUIT 0011;

COLLECT STATS INDEX (Te_rut)
ON EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_AUM_ESTIMADO;
.IF ERRORCODE <> 0 THEN .QUIT 0012;

/* **********************************************************************/
/* 			SE CREA TABLA PRELIMINAR CON TODOS LOS CAMPOS NECESARIOS  */
/*              PARA EL PROCESO DE  PERFILAMIENTO Y SEGMENTACION                  */
/* **********************************************************************/
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEGMENTACION_PRE ;
CREATE MULTISET TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEGMENTACION_PRE
     (
      Te_rut INTEGER,
      Te_party_id INTEGER,
      Te_edad INTEGER,
      Te_fecha_ref_aum INTEGER,
      Te_MODELO_ID INTEGER,
      Td_PATRIMONIO_EST DECIMAL(18,4),
      Te_fecha_ref_inr INTEGER,
      Te_potencial_ciclo_cli FLOAT,
      Tc_grupo_potencial_ciclo VARCHAR(12) CHARACTER SET LATIN NOT CASESPECIFIC,
      Tc_ESTRATEGIA VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
      Tc_Rango_PI VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
      Tc_PasaFiltrosDuros VARCHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      Tc_PasaFiltrosMenosSeveros VARCHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC)
PRIMARY INDEX ( Te_rut );
.IF ERRORCODE <> 0 THEN .QUIT 0013;

/* **********************************************************************/
/* 			SE INSERTA TABLA PRELIMINAR CON TODOS LOS CAMPOS NECESARIOS  */
/*              PARA EL PROCESO DE  PERFILAMIENTO Y SEGMENTACION                  */
/* **********************************************************************/
INSERT INTO EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEGMENTACION_PRE
	SELECT A.pe_rut AS te_rut
  , A.pe_party_id AS te_party_Id
  , A.pe_edad AS Te_edad
  , B.Te_fecha_ref AS fecha_ref_aum
  , B.Te_MODELO_ID
  , B.Td_valor AS Td_PATRIMONIO_EST
  , C.Te_fecha_ref AS Te_fecha_ref_inr
  , C.Te_potencial_ciclo_cli
  , C.Tc_grupo_potencial_ciclo
  , D.tc_estrategia AS Tc_ESTRATEGIA
  , D.tc_rango_pi AS Tc_Rango_PI
  , D.tc_pasafiltrosduros AS Tc_PasaFiltrosDuros
  , D.tc_pasafiltrosmenosseveros AS Tc_PasaFiltrosMenosSeveros
	FROM edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct A
	LEFT JOIN EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_AUM_ESTIMADO B ON A.pe_rut = B.Te_rut
	LEFT JOIN EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_INR_POTENCIAL_CICLO C ON A.pe_rut = C.Te_rut
	LEFT JOIN EDW_TEMPUSU.T_{{next_ds_nodash}}_riesgo1 D ON A.pe_rut = D.te_rut -- modificacion hecha el 202109 - v3.2 -- ahora es un historico
	;
.IF ERRORCODE <> 0 THEN .QUIT 0014;

COLLECT STATS INDEX (Te_rut)
ON EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEGMENTACION_PRE;
.IF ERRORCODE <> 0 THEN .QUIT 0015;

/* **********************************************************************/
/* SE CREA TABLA DE RENTA DE LOS CLIENTES  USANDO  */
/* LA FUENTE INFORMADA POR CLIENTES Y UNA ESTIMACION 	*/
/* **********************************************************************/
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_RENTA ;
CREATE MULTISET TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_RENTA
     (
      Te_rut INTEGER,
      Td_mix_renta FLOAT)
PRIMARY INDEX ( Te_rut );
.IF ERRORCODE <> 0 THEN .QUIT 0016;

/* **********************************************************************/
/* SE INSERTA TABLA DE RENTA DE LOS CLIENTES  USANDO  */
/* LA FUENTE INFORMADA POR CLIENTES Y UNA ESTIMACION 	*/
/* **********************************************************************/
INSERT INTO EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_RENTA
	SELECT A.pe_Rut
	, C.Rta_EstBCI_Final AS Td_mix_renta
	FROM {{ var.value.BD_ANALYTICS}}.MP_CliRtaEstBCI_HIST C
  INNER JOIN edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct A
    ON A.pe_rut = C.rut
  INNER JOIN edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec D
    ON C.fecha_ref = D.pc_fecha_ref_m2
;
.IF ERRORCODE <> 0 THEN .QUIT 0017;

COLLECT STATS INDEX (Te_rut)
ON EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_RENTA;
.IF ERRORCODE <> 0 THEN .QUIT 0018;


/* **********************************************************************/
/*  CALCULAMOS INR USANDO TABLA DE GESTION   */
/* **********************************************************************/
-- Calculo como promedio de los ultimos 6 meses
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion_ult6m ;
CREATE MULTISET TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion_ult6m
     (
      tc_fecha_ref CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
      te_rut INTEGER,
      td_inr_mes DECIMAL(38,4))
PRIMARY INDEX ( tc_fecha_ref ,te_rut );
.IF ERRORCODE <> 0 THEN .QUIT 0019;

INSERT INTO EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion_ult6m
	SELECT
	CAST(A.Fec_periodo AS DATE FORMAT 'YYYYMMDD')(CHAR(6)) AS tc_fecha_ref
	, A.cli_rut AS te_rut
	, SUM(A.Mto_Margen_Bruto + A.Mto_Gasto_Riesgo) AS td_inr_mes
	FROM edw_vw.BCI_RCP_GST A
	INNER JOIN edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec B
		ON CAST(A.Fec_periodo AS DATE FORMAT 'YYYYMMDD')(CHAR(6)) <= B.pc_fecha_ref_M1
		AND CAST(A.Fec_periodo AS DATE FORMAT 'YYYYMMDD')(CHAR(6)) >=  CAST(ADD_MONTHS( pf_fecha_Ini , -8) AS DATE FORMAT 'YYYYMMDD')(CHAR(6)) -- pc_fecha_ref_M8, no existe asi que la creamos
	WHERE cli_rut <= 40000000
	QUALIFY ROW_NUMBER() OVER (PARTITION BY A.cli_rut ORDER BY CAST(A.Fec_periodo AS DATE FORMAT 'YYYYMMDD')(CHAR(6)) DESC) <= 6
	GROUP BY
		 CAST(A.Fec_periodo AS DATE FORMAT 'YYYYMMDD')(CHAR(6))
		, A.cli_rut
;
.IF ERRORCODE <> 0 THEN .QUIT 0020;

COLLECT STATS INDEX (tc_fecha_ref, te_rut)
ON EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion_ult6m;
.IF ERRORCODE <> 0 THEN .QUIT 0021;

DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion;
CREATE MULTISET TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion
     (
      tc_fecha_ref CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
      te_rut INTEGER,
      td_inr_6m FLOAT,
      te_n_periodos INTEGER)
PRIMARY INDEX ( tc_fecha_ref ,te_rut );
.IF ERRORCODE <> 0 THEN .QUIT 0022;

INSERT INTO EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion
	SELECT tc_fecha_ref
	, te_rut
	, AVG(td_inr_mes) OVER (ORDER BY te_rut, tc_fecha_ref ROWS BETWEEN 5 PRECEDING AND 0 PRECEDING) AS td_inr_6m
	, COUNT(td_inr_mes) OVER (ORDER BY te_rut, tc_fecha_ref ROWS BETWEEN 5 PRECEDING AND 0 PRECEDING) AS te_n_periodos
	FROM EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion_ult6m A
	QUALIFY ROW_NUMBER() OVER(PARTITION BY te_rut ORDER BY tc_fecha_ref DESC) = 1
;
.IF ERRORCODE <> 0 THEN .QUIT 0023;

COLLECT STATS INDEX (tc_fecha_ref, te_rut)
ON EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion;
.IF ERRORCODE <> 0 THEN .QUIT 0024;

/* **********************************************************************/
/* SE CREA TABLA CON EL PERFIL DE LOS CLIENTES, BASADO EN   */
/* RENTA, AUM  Y EDAD	*/
/* **********************************************************************/
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_PRELIMINAR ;
CREATE MULTISET TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_PRELIMINAR
     (
      Te_rut INTEGER,
      Te_edad INTEGER,
      Te_party_id INTEGER,
      td_INR_sistema FLOAT,
      td_potencial_ciclo_cli FLOAT,
      tc_grupo_potencial_ciclo VARCHAR(12) CHARACTER SET LATIN NOT CASESPECIFIC,
      td_inr_6m FLOAT,
      Tc_Rango_PI VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
      Tc_PasaFiltrosDuros VARCHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      Td_mix_renta FLOAT,
      tc_perfil_cliente VARCHAR(2) CHARACTER SET UNICODE NOT CASESPECIFIC,
      Tc_info_usada_perfil VARCHAR(10) CHARACTER SET UNICODE NOT CASESPECIFIC,
      Td_vinculacion_inr FLOAT)
PRIMARY INDEX ( Te_rut );
.IF ERRORCODE <> 0 THEN .QUIT 0025;

/* **********************************************************************/
/* SE INSERTA TABLA CON EL PERFIL DE LOS CLIENTES, BASADO EN   */
/* RENTA, AUM  Y EDAD	*/
/* **********************************************************************/
INSERT INTO EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_PRELIMINAR
	SELECT A.Te_rut
		, A.Te_edad
		, A.Te_party_id
		-- Aplicamos logicas para ajustar el potencial
		, D.Td_INR_sistema -- antes: td_potencial_inr_ajus
    , A.te_potencial_ciclo_cli
    , A.tc_grupo_potencial_ciclo
    , C.td_inr_6m
		, A.Tc_rango_pi
    , A.Tc_PasaFiltrosDuros
		, B.Td_mix_renta
    -- modificacion hecha el 202104 - v3.1
    -- CORTES ENTRE PATRIMONIO Y RENTA, si cumple alguna de las dos condiciones de renta o aum queda en ese segmento queda ahi
    -- en la condicion de AUM dejamos 1 a 15000 poruq el modelo de AUM deja todos los valores en 0 cuando es menor a cierto monto
    , CASE WHEN A.Te_edad >= 60 AND (A.Td_PATRIMONIO_EST >= 50000 OR B.Td_mix_renta >= 3000.00) THEN 'A1'
           WHEN A.Te_edad >= 60 AND (A.Td_PATRIMONIO_EST >= 15000 OR B.Td_mix_renta >= 1300.00) THEN 'B1'
           WHEN A.Te_edad >= 60 AND (A.Td_PATRIMONIO_EST >= 0 OR B.Td_mix_renta >= 0) THEN 'C1'
           WHEN A.Te_edad >= 60 THEN 'C1' -- caso donde no tenemos informacion de renta o patrimonio
           WHEN A.Te_edad >= 30 AND (A.Td_PATRIMONIO_EST >= 50000 OR B.Td_mix_renta >= 3000.00) THEN 'A2'
           WHEN A.Te_edad >= 30 AND (A.Td_PATRIMONIO_EST >= 15000 OR B.Td_mix_renta >= 1300.00) THEN 'B2'
           WHEN A.Te_edad >= 30 THEN 'C2' -- caso donde no tenemos informacion de renta o patrimonio
           WHEN A.Te_edad >= 30 AND (A.Td_PATRIMONIO_EST >= 0 OR B.Td_mix_renta >= 0) THEN 'C2'
           WHEN A.Te_edad >= 18 AND (A.Td_PATRIMONIO_EST >= 15000 OR B.Td_mix_renta >= 1300.00) THEN 'B3'
           WHEN A.Te_edad >= 18 AND (A.Td_PATRIMONIO_EST >= 0 OR B.Td_mix_renta >= 0.00) THEN 'C3'
           WHEN A.Te_edad >= 18 THEN 'C3' -- caso donde no tenemos informacion de renta o patrimonio
           -- casos de null son porque no hay data de la edad
           END tc_perfil_cliente
		,CASE WHEN A.Td_PATRIMONIO_EST <> 0 THEN 'patrimonio'
						WHEN A.Td_PATRIMONIO_EST = 0  THEN 'renta'
						END Tc_info_usada_perfil
		,D.td_vinculacion_inr
	FROM  EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEGMENTACION_PRE A
	LEFT JOIN EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_RENTA B
		ON A.Te_rut = B.Te_rut
	LEFT JOIN EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion C
		ON A.te_rut = C.te_rut
  LEFT JOIN EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_INR_SISTEMA D
    ON A.te_rut = D.te_rut
;
.IF ERRORCODE <> 0 THEN .QUIT 0026;

COLLECT STATS INDEX (Te_rut)
, COLUMN (Tc_perfil_cliente)
ON EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_PRELIMINAR;
.IF ERRORCODE <> 0 THEN .QUIT 0027;

/* **********************************************************************/
/* SE CREA TABLA FINAL TEMPORAL CON EL PERFILAMIENTO Y SEFGMENTACION  */
/* **********************************************************************/
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_FINAL ;
CREATE MULTISET TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_FINAL
     (
      Tc_fecha_ref CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
      Te_rut INTEGER,
      Te_edad INTEGER,
      Te_party_id INTEGER,
      Tc_perfil_cliente VARCHAR(2) CHARACTER SET UNICODE NOT CASESPECIFIC,
      Td_inr_sistema FLOAT, -- incorporado el inr sistema
      Td_inr_potencial_ciclo FLOAT, -- antes: Td_potencial_INR
      td_inr_avg6m FLOAT,
      Td_vinculacion_inr FLOAT,
      Tc_segmento VARCHAR(37) CHARACTER SET UNICODE NOT CASESPECIFIC,
      Tc_estrategia VARCHAR(37) CHARACTER SET UNICODE NOT CASESPECIFIC,
      Te_tiene_inr_neg BYTEINT)
PRIMARY INDEX ( Te_rut );
.IF ERRORCODE <> 0 THEN .QUIT 0028;

/* **********************************************************************/
/* SE INSERTA TABLA FINAL TEMPORAL CON EL PERFILAMIENTO Y SEFGMENTACION  */
/* **********************************************************************/
INSERT INTO EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_FINAL
	SELECT C.pc_fecha_ref
	, A.Te_rut
	, A.Te_edad
	, A.Te_party_id
	, A.Tc_perfil_cliente
  , CASE WHEN A.Td_inr_sistema < A.td_inr_6m THEN A.td_inr_6m ELSE A.Td_inr_sistema END  AS Td_inr_sistema  -- modificado 202104 v3.1 - parche
	, A.td_potencial_ciclo_cli
	, A.td_inr_6m  -- inr basado en margen bruto, no con margen financiero
	, A.Td_vinculacion_inr
	, CASE WHEN A.tc_grupo_potencial_ciclo = 'Alto Valor'  AND (A.Tc_rango_pi = 'NO CUMPLE PI' OR A.Tc_PasaFiltrosDuros = 'NO') THEN 'No cumple PI - Alto potencial inr'  -- modificacion v3.1 - incorporamos filtros duros
  		 	 WHEN A.tc_grupo_potencial_ciclo = 'Bajo Valor' AND (A.Tc_rango_pi = 'NO CUMPLE PI' OR A.Tc_PasaFiltrosDuros = 'NO') THEN 'No cumple PI - Bajo potencial inr'  -- modificacion v3.1 - incorporamos filtros duros
  			 WHEN A.tc_grupo_potencial_ciclo = 'Alto Valor'  AND A.Td_vinculacion_inr >= B.corte_vinculacion THEN 'Alto potencial INR - Alta vinculacion'
  			 WHEN A.tc_grupo_potencial_ciclo = 'Alto Valor'  AND A.Td_vinculacion_inr < B.corte_vinculacion THEN 'Alto potencial INR - Baja vinculacion'
  			 WHEN A.tc_grupo_potencial_ciclo = 'Bajo Valor' AND A.Td_vinculacion_inr >= B.corte_vinculacion THEN 'Bajo potencial INR - Alta vinculacion'
  			 WHEN A.tc_grupo_potencial_ciclo = 'Bajo Valor' AND A.Td_vinculacion_inr < B.corte_vinculacion THEN 'Bajo potencial INR - Baja vinculacion'
  			 ELSE 'Otro'
  			 END segmento
  , CASE WHEN segmento = 'Alto potencial INR - Alta vinculacion' THEN 'Blindar'
				 WHEN segmento = 'Alto potencial INR - Baja vinculacion' THEN 'Vincular'
			   WHEN segmento = 'Bajo potencial INR - Alta vinculacion' THEN 'Rentabilizar'
				 WHEN segmento = 'Bajo potencial INR - Baja vinculacion' THEN 'Eficientar'
				 WHEN segmento = 'No cumple PI - Bajo potencial inr' THEN 'Riesgo - bajo potencial'
				 WHEN segmento = 'No cumple PI - Alto potencial inr' THEN 'Riesgo - alto potencial'
				 ELSE NULL
				 END tc_estrategia
	, CASE WHEN A.td_inr_6m < 0 THEN 1
			WHEN A.td_inr_6m >= 0 THEN 0
			END Te_tiene_inr_neg
	FROM EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_PRELIMINAR A
	LEFT JOIN {{ var.value.BD_ANALYTICS}}.C_CORTES_NEG_SEG_EST_PER B
		ON A.Tc_perfil_cliente = B.perfil_cliente
	LEFT JOIN  edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec C ON 1 = 1;
.IF ERRORCODE <> 0 THEN .QUIT 0029;

COLLECT STATS INDEX (Te_rut)
ON EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_FINAL;
.IF ERRORCODE <> 0 THEN .QUIT 0030;

/* **********************************************************************/
/* SE VACIA LA TABLA FINAL */
/* **********************************************************************/
DELETE  {{ var.value.BD_ANALYTICS}}.I_AP_SEG_EST_CLI_BCI ALL;
.IF ERRORCODE <> 0 THEN .QUIT 0031;

/* **********************************************************************/
/* INSERTAMOS LOS VALORES DE PERFILAMIENTO Y  SEGMENTACION EN*/
/* LA TABLA FINAL	*/
/* **********************************************************************/
INSERT INTO {{ var.value.BD_ANALYTICS}}.I_AP_SEG_EST_CLI_BCI
  SELECT tc_fecha_ref
  , te_rut
  , te_edad
  , te_party_id
  , tc_perfil_cliente
  , td_inr_avg6m
  , td_inr_sistema
  , td_inr_potencial_ciclo
  , td_vinculacion_inr
  , tc_segmento
  , tc_estrategia
  , te_tiene_inr_neg
	FROM EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_FINAL;
.IF ERRORCODE <> 0 THEN .QUIT 0032;

COLLECT STATS INDEX (ic_fecha_ref , ie_rut)
ON {{ var.value.BD_ANALYTICS}}.I_AP_SEG_EST_CLI_BCI;
.IF ERRORCODE <> 0 THEN .QUIT 0033;

/* **********************************************************************/
/* BORRAMOS EL PERIODO A CARGAR  POR UN POSIBLE REPROCESO   */
/* **********************************************************************/
DELETE FROM {{ var.value.BD_ANALYTICS}}.I_AP_SEG_EST_CLI_BCI_HIST
WHERE Ic_fecha_ref= (SELECT MAX(Tc_fecha_ref) FROM EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_FINAL);

.IF ERRORCODE <> 0 THEN .QUIT 0034;
/* **********************************************************************/
/* SE INSERTA LA INFORMACION DE SEGMENTACION Y PERFILAMIENTO EN   */
/* LA TABLA FINAL HISTORICA	*/
/* **********************************************************************/
INSERT INTO {{ var.value.BD_ANALYTICS}}.I_AP_SEG_EST_CLI_BCI_HIST
	SELECT tc_fecha_ref
  , te_rut
  , te_edad
  , te_party_id
  , tc_perfil_cliente
  , td_inr_avg6m
  , td_inr_sistema
  , td_inr_potencial_ciclo
  , td_vinculacion_inr
  , tc_segmento
  , tc_estrategia
  , te_tiene_inr_neg
	FROM EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_FINAL;
.IF ERRORCODE <> 0 THEN .QUIT 0035;

COLLECT STATS INDEX (ic_fecha_ref , ie_rut)
ON {{ var.value.BD_ANALYTICS}}.I_AP_SEG_EST_CLI_BCI_HIST;
.IF ERRORCODE <> 0 THEN .QUIT 0036;

/* **********************************************************************/
/* SE ELIMINAN LAS TABLAS TEMPORALES DENTRO DEL PROCESO   */
/* **********************************************************************/
DROP TABLE edw_tempusu.P_{{next_ds_nodash}}_segmentacion_fecha_ejec;
DROP TABLE edw_tempusu.P_{{next_ds_nodash}}_per_rutero_po_cct;
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_AUM_ESTIMADO;
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_INR_SISTEMA;
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_INR_POTENCIAL_CICLO;
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEGMENTACION_PRE;
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_MAX_FECHA_TABLON_ANALITICO;
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_RENTA;
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_PRELIMINAR;
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion_ult6m;
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_inr_by_cli_gestion;
DROP TABLE EDW_TEMPUSU.T_{{next_ds_nodash}}_PER_TBL_SEG_EST_FINAL; -- Comentar para verificar que se inserta

---- FIN TABLON
SELECT DATE, TIME;
.QUIT 0;
