# Segmentación estratégica v3 #
### Actualizado al 2020/10/26 ###

Este proceso genera una segmentación para los clientes cuenta correntistas del banco.

Para esto realiza primero un perfilamiento basado en dos dimensiones: (1) edad , (2) Potencial AUM o Renta, esto da origen a los perfiles:

* A1: Alto bienestar económico, mayor a 60
* A2: Alto bienestar económico, entre 30 y 60
* B1: Medio bienestar económico, mayor a 60
* B2: Medio bienestar económico, entre 30 y 60
* B3: Medio bienestar económico, entre 18 y 30
* C1: Bajo bienestar económico, mayor a 60
* C2: Bajo bienestar económico, entre 30 y 60
* C3: Bajo bienestar económico, entre 18 y 30

En segundo lugar, este activo realiza una segmentanción basada en (1) La vinculación del cliente, (2) el potencial de ciclo del cliente, (3) Si cumple el corte de la PI (Política de incumplimiento), esto da origen a los segmentos:

* Alto potencial, vinculado
* Alto potencial, no vinculado
* Bajo potencial, vinculado
* Bajo potencial, no vinculado
* Alto potencial, no cumple PI
* Bajo potencial, no cumple PI
* Otros (Cuando no hay datos de gestión)

### ¿Cómo puedo saber si la inforamción está actualizada? ###
En esta caso si hoy es 202011, lo esperable es que la tabla tenga informacion al 202011. Esto es independiente del desfase que tienen la data de input (Ejemplo: información de gestión, potencial inr o inr mercado).

### ¿Qué es el potencial de Ciclo? ###
El potencial de ciclo es un potencial de INR, que trae a valor presente los flujos de INR futuros de un cliente, con un horizonte de 5 años. Parra estimar los flujos futuros se hace a partir de una lógica de gemelos, basado en clientes más vinculados.

### ¿Qué es el INR de mercado? ###
Es el INR que estimamos que el cliente tiene en sistema, mirando información de la CMF y el INR real que el cliente genera en BCI. Se calcula a través de un activo analítico.

### ¿Cómo se define la vinculación? ###
La vinculación se interpreta como el porcentaje del INR de sistema que capturamos en BCI, se construye utilizando el INR sistema y el INR en BCI. Para el INR sistema se utiliza información de gestión, mientras que el INR sistema es una estimación hecha por un modelo de analytics.

### ¿Alguna consideración para reprocesar historia? ###
Se debe modificar el código y usar la fuente historica de la riesgo1 (Mkt_Crm_Cmp_Tb.in_riesgo1_AAAAMM). Para esto se debe utilizar el mismo periodo que se está ejecutando, es decir, si hay que reprocesar el 202001, entonces la tabla a utilizar es Mkt_Crm_Cmp_Tb.in_riesgo1_202001. Para ejecuciones actuales del proceso el código debe estar con ña tabla actual de BCIMKT.riesgo1

### ¿Dónde hay presentaciones respecto a este activo? ###
1. [Segmentación estratégica v3](https://docs.google.com/presentation/d/1FKZBuLT-ZK6iXP6x1HxsLvGMjeD4oOpKVbPxxsVeXM0/edit#slide=id.g6ba96e58bc_0_810)
2. [Segmentación estratégica v2](https://docs.google.com/presentation/d/17b6Rzt4_ek6VzGx54rixLC55yMDQcv0DL_2dwH8m180/edit#slide=id.g6ba96e58bc_0_810)
