# Autor: Bruno Gomez
# Modificador: Bruno Gomez
# Fecha: Febrero 2021
# Descripcion: AP_302_Segmentacion_Estrategica_v3
# Version: v3.1
# Migrado a Cloud por: CCC
# Fecha: 10/2021

from airflow import DAG, utils
from airflow.providers.teradata.operators.bteq import BteqOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.email_operator import EmailOperator
from airflow.models import Variable
from datetime import datetime, timedelta, date, time
import logging
import os
import pendulum


"""
Inicio de configuracion basica del DAG
"""
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
lista_correops = Variable.get("lista_correops", "camilo.carrascoc@bci.cl")

default_args = {
    'owner': 'Analytics-Persona',
    'start_date':  utils.dates.days_ago(35).astimezone(pendulum.timezone(Variable.get('TZ'))),
    'email': lista_correops,
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 6,
    'retry_delay': timedelta(minutes=5)
}
"""
Fin de configuracion basica del DAG
"""

dag = DAG('AP_302_Segmentacion_Estrategica_Nueva', default_args=default_args, schedule_interval="30 14 2 * *",tags=['AP','Activo Analitico'])


t0 = DummyOperator(
    task_id='Inicio_14_30_AM',
    dag=dag
)


dag_tasks = [t0]

def define_bteq_op(bteq_file, bteq_name, bteq_params={}):
    """ Define operador BTEQ para ejecutar en contenedor"""
    return BteqOperator(
        bteq=os.path.join(queries_folder, os.path.basename(bteq_file)),
        task_id=bteq_name,
        ttu_conn_id='td_ttu',
        pool='td_pool',
        depends_on_past=False,
        dag=dag)

import glob

#Mail Operator
Ejecuta_Mail_Operator = EmailOperator(
    task_id='Mail_Fin',
    to=lista_correops,
    subject='Fin Segmentacion Estrategica',
    html_content="Estimados, Se informa la finalizacion del DAG AP_302_Segmentacion_Estrategica",
    dag=dag)

queries_folder = 'BTEQs'
ext_file = '.sql'
bteq_files = [f for f in glob.glob('%s/*%s' % (os.path.join(__location__, queries_folder), ext_file)) if
              f.endswith(ext_file)]

for bteq_file in sorted(bteq_files):
    try:
        query_task_id = os.path.basename(bteq_file).split(ext_file)[0]
    except:
        query_task_id = str(uuid.uuid4())
        logging.info("Archivo con el nombre malo : " + bteq_file)
        pass
    dag_tasks.append(define_bteq_op(bteq_file, query_task_id))

# Definiendo dependencias, se asume secuencialidad
for seq_pair in zip(dag_tasks, dag_tasks[1:]):
    seq_pair[0] >> seq_pair[1]

seq_pair[1] >> Ejecuta_Mail_Operator
