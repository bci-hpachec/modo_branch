
# coding: utf-8

# Librerias y Utilitarios

from __future__ import division
import seaborn as sns # mejores gr�ficosc
import warnings
import giraffez
import pandas as pd
import numpy as np
import xgboost
from sklearn.pipeline import Pipeline, FeatureUnion
from xgboost import XGBRegressor
from xgboost import XGBClassifier
from xgboost import plot_importance
import category_encoders as ce
from sklearn.preprocessing import Imputer, FunctionTransformer
from sklearn.base import TransformerMixin
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
#from bciutils.beta_v2.transform.discretizer import Discretizador
from sklearn.metrics import confusion_matrix
#get_ipython().magic('matplotlib inline')
import matplotlib.pyplot as plt
import numpy as np
from sklearn.externals import joblib
import csv

np.set_printoptions(precision=2)

def impute(X):
    return X.fillna(-99999999999)

class ColumnWatcher(TransformerMixin):
    def fit(self, X, y=None):
        self.columns = list(X.columns)
        return self

    def transform(self, X, y=None):
        return X

# Carga de datos

td_config = {
    "username": "usr_an_common",
    "password": "anlc1812",
    "host": "dataware.bci.cl"}

with giraffez.BulkExport("SELECT a.fecha_ref, a.RUT,a.PARTY_ID, a.ult_endeud_sbif,a.inv_prom_saldo_total_3m,a.edad,		a.ind_viajechip_3m,a.renta_interna,a.ratio_deuda_hip_sbif_1m_6m,a.es_empresario_bci,		a.sum_avaluo,a.avg_saldo_cct_1m,a.inv_max_saldo_total_3m,a.med6m_marg_glob_bci,		a.ant_primera_cct,a.inv_max_saldo_total_6m,a.renta,inv_min_saldo_total_6m,		a.inv_min_saldo_total_3m,a.max_12m_deu_com_bci_sbif,a.capital_total_dap,		a.max_12m_disp_con_bci,a.deuda_con_sbif_vs_renta,a.cupo_lc_sistema,		a.din_cons_suc,a.din_fuera_comuna,a.din_cons_prof,a.din_fuera_suc,a.max_saldo_cct_12m,		a.max_saldo_cct_3m,a.ratio_mto_abono_cct_1m_6m,a.recencia_cierre_cpr,		a.comuna,a.din_cons_comuna,a.inv_max_saldo_total_12m,a.estimacion_renta_tot,		a.mto_transf_si_mismo_6m,a.max_saldo_cct_1m,a.max_saldo_lc_12m,a.evol_deuda_hip_sbif_3m_6m,		a.total_bienes_mm,a.n_empresas_base_fuera,a.evol_deu_con_fuera_sbif_6m_12m,		a.evol_mto_abono_cct_6m_12m,a.avg_campanias_tc_ejecutivo_1m,a.ant_primer_hip,		a.ant_primera_lc,a.sit_laboral, a.suc_a_aad FROM MKT_CRM_ANALYTICS_TB.mp_bci_tablon_analitico A INNER JOIN BCIMKT.MP_INV_CCT_RUTERO B 	ON A.RUT = B.RUT 	AND B.FECHA_REF = EXTRACT(YEAR FROM ADD_MONTHS(CURRENT_DATE,-1))*100 + EXTRACT(MONTH FROM ADD_MONTHS(CURRENT_DATE,-1))  	AND (B.ES_INV = 1 	OR B.ES_CCT = 1)"
                         ""
                         , **td_config) as export:
    data = pd.DataFrame.from_dict(export.to_dict())
data.to_pickle('dataset_cct.pkl') # dejar backup

## Carga Modelos

clf_ten = joblib.load('modelo_tenencia.pkl')
clf_15 = joblib.load('modelo_15.pkl')
clf_15_50 = joblib.load('modelo_15_50.pkl')
clf_50 = joblib.load('modelo_50.pkl')
regresor = joblib.load('modelo_regresor_final.pkl')

# ## Aplicacion

data_app = pd.read_pickle('dataset_cct.pkl') # cargar backup

X = data_app[['ult_endeud_sbif','inv_prom_saldo_total_3m','edad','ind_viajechip_3m','renta_interna','ratio_deuda_hip_sbif_1m_6m','es_empresario_bci','sum_avaluo','avg_saldo_cct_1m','inv_max_saldo_total_3m','med6m_marg_glob_bci','ant_primera_cct','inv_max_saldo_total_6m','renta','inv_min_saldo_total_6m','inv_min_saldo_total_3m','max_12m_deu_com_bci_sbif','capital_total_dap','max_12m_disp_con_bci','deuda_con_sbif_vs_renta','cupo_lc_sistema','din_cons_suc','din_fuera_comuna','din_cons_prof','din_fuera_suc','max_saldo_cct_12m','max_saldo_cct_3m','ratio_mto_abono_cct_1m_6m','recencia_cierre_cpr','comuna','din_cons_comuna','inv_max_saldo_total_12m','estimacion_renta_tot','mto_transf_si_mismo_6m','max_saldo_cct_1m','max_saldo_lc_12m','evol_deuda_hip_sbif_3m_6m','total_bienes_mm','n_empresas_base_fuera','evol_deu_con_fuera_sbif_6m_12m','evol_mto_abono_cct_6m_12m','avg_campanias_tc_ejecutivo_1m','ant_primer_hip','ant_primera_lc','sit_laboral']]

# ### Predicciones

PRED_TEN = pd.DataFrame({"tenencia": clf_ten.predict_proba(X)[:,1]})
PRED_T15 = pd.DataFrame({"T15": clf_15.predict_proba(X)[:,1]})
PRED_T15_50 = pd.DataFrame({"T15_50": clf_15_50.predict_proba(X)[:,1]})
PRED_T50 = pd.DataFrame({"T50":clf_50.predict_proba(X)[:,1]})
MAX_SALDO  = pd.DataFrame({"Max_Saldo":data_app['inv_max_saldo_total_12m']})
SUC_A_AAD = pd.DataFrame({"SUC_A_AAD":data_app["suc_a_aad"]})

rutero = pd.DataFrame({"rut":data_app["rut"]})

### Tablon 1: Tablon Probabilidades

Tablon1 = pd.merge(rutero, PRED_TEN, right_index=True, left_index= True)
Tablon1 = pd.merge(Tablon1, PRED_T15, right_index=True, left_index= True)
Tablon1 = pd.merge(Tablon1, PRED_T15_50, right_index=True, left_index= True)
Tablon1 = pd.merge(Tablon1, PRED_T50, right_index=True, left_index= True)

### Tablon 2: Tablon Montos

rutero2 = pd.DataFrame({"rut":Tablon1["rut"]})
aum_estimado = pd.DataFrame({"AUM_ESTIMADO": regresor.predict(Tablon1[["T15","T15_50","T50"]])})
tenencia = pd.DataFrame({"Tenencia": Tablon1['tenencia']})

Tablon2 = pd.merge( rutero2,aum_estimado, right_index=True, left_index= True,how='left')
Tablon2 = pd.merge(Tablon2,MAX_SALDO , right_index=True, left_index= True,how='left')
Tablon2 = pd.merge(Tablon2, tenencia, right_index=True, left_index= True,how='left')

# ### Tablon 3: Tablon Carga Teradata

## Correcciones Tenencia

#1 Correcciones max inversion
Tablon2['Tenencia_corr'] =  np.where(Tablon2['Max_Saldo'] > 0, 1, Tablon2['Tenencia'])
#2 Correccion por SUC
#Tablon2['Tenencia_corr'] =  np.where(Tablon2['SUC_A_AAD'] > 0, 1, Tablon2['Tenencia_corr']) -- problema en correccion
#3 Imputa tenencia 1 a clientes con probabilidad mayor al 20% y cero al resto
Tablon2['Tenencia_corr'] =  np.where(Tablon2['Tenencia_corr'] < 0.2, 0, 1)

## Correcciones AUM
#1 Aplicacion Modelo Tenencia
Tablon2['AUM_ESTIMADO_Corr'] =  np.where(Tablon2['AUM_ESTIMADO'] < 50000, Tablon2['AUM_ESTIMADO']*Tablon2['Tenencia_corr'], Tablon2['AUM_ESTIMADO'] )
#2 Valores AUM_ESTIMADO_Corr
Tablon2['AUM_ESTIMADO_Corr'] =  np.where(Tablon2['AUM_ESTIMADO_Corr'] < 0, 0, Tablon2['AUM_ESTIMADO_Corr'] )
#3 Saldos Inversion Mayor a AUM
Tablon2['AUM_ESTIMADO_Corr'] =  np.where(Tablon2['AUM_ESTIMADO_Corr'] < Tablon2['Max_Saldo'], Tablon2['Max_Saldo'], Tablon2['AUM_ESTIMADO_Corr'] )
#4 SUC Mayor a AUM
#Tablon2['AUM_ESTIMADO_Corr'] =  np.where(Tablon2['AUM_ESTIMADO_Corr'] < Tablon2['SUC_A_AAD'], Tablon2['SUC_A_AAD'], Tablon2['AUM_ESTIMADO_Corr'] ) -- problema en correccion

## Renombre de campos

Tablon2['rut'] = Tablon2['rut'].astype(int)
Tablon2['Fecha_Ref'] =  data_app.fecha_ref
Tablon2['MODELO_ID'] =  11
Tablon2['valor'] = Tablon2['AUM_ESTIMADO_Corr'].astype(int)

Tablon_Final = Tablon2[['rut','Fecha_Ref','MODELO_ID','valor']]

### Carga En Teradata

Tablon_Final.to_csv('Tablon_Final_CCT.csv', index=False, sep='|', na_rep='NULL')

# ### Carga Sobre Teradata

with giraffez.BulkLoad("EDW_TEMPUSU.SG_MOD_AUM_CLI", **td_config) as load: # giraffez
    load.cleanup()
    with open("Tablon_Final_CCT.csv", 'r') as csvfile: # lector csv
        reader = csv.reader(csvfile, delimiter='|', quotechar='"')
        next(reader) ## skip header # saltar primera linea
        for i, row in enumerate(reader):
            try:
                load.put(row)
            except Exception as e:
                print("Error %s in row %s" % (repr(e), i))
