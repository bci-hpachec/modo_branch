/*********************************************************************
**********************************************************************
** DSCRPCN: COPIA TABLAS PARA LA CARGA FINAL MODELOS VIA AIRFLOW    **
**																	**
** AUTOR  : CCC                                                     **
** EMPRESA: BCI                                                     **
** FECHA  : 04/2019                                                 **
*********************************************************************/
/*********************************************************************
** MANTNCN:                                                         **
** AUTOR  :                                                         **
** FECHA  : SSAAMMDD                                                **
/*********************************************************************
** TABLA ENTRADA: 	EDW_TEMPUSU.SG_MOD_AUM_CLI		    	        **
** TABLA FINAL  :	Mkt_Crm_Analytics_Tb.MP_INV_PROB_HIST   	                    **
**********************************************************************
*********************************************************************/
.SET SESSION CHARSET 'UTF8';
SELECT DATE, TIME;

/* **********************************************************************/
/* 			         SE EJECUTA LA COPIA DE TABLAS                     */
/* **********************************************************************/
--BORRAMOS LA MAXIMA FECHA PARA UN REPROCESO

DELETE FROM Mkt_Crm_Analytics_Tb.MP_INV_PROB_HIST_PRUEBA WHERE FECHA_REF = extract(year from current_date)*100 + extract(month from current_date) and MODELO_ID = 11;

.IF ERRORCODE <> 0 THEN .QUIT 1;

INSERT INTO Mkt_Crm_Analytics_Tb.MP_INV_PROB_HIST_PRUEBA
select * from EDW_TEMPUSU.SG_MOD_AUM_CLI;
.IF ERRORCODE <> 0 THEN .QUIT 3;

INSERT INTO MKT_CRM_ANALYTICS_TB.R_MODELO_EJECUCION
SELECT CURRENT_TIMESTAMP
	,'TERMINO'
	,'406_Ejecutable_Modelo_AUM_v1'
	,'Analytics_Persona';

INSERT INTO MKT_CRM_ANALYTICS_TB.R_MODELOS_RESULTADOS
SELECT CURRENT_TIMESTAMP
	,'406_Ejecutable_Modelo_AUM_v1'
	,'Analytics_Persona'
	,'MKT_CRM_ANALYTICS_TB.MP_INV_PROB_HIST_PRUEBA'
	,'Mensual'
	,CAST(MAX(Fecha_Ref) AS VARCHAR(6))
	,COUNT(1) as N
FROM MKT_CRM_ANALYTICS_TB.MP_INV_PROB_HIST_PRUEBA
WHERE MODELO_ID = 11;

SELECT DATE, TIME;
.quit 0;
