# -*- coding: utf-8 -*-
import sys
from importlib import reload

reload(sys)
#sys.setdefaultencoding('utf-8')

# Modificador: CCC
# Fecha: 05 de Marzo 2019
# Autor: GOB
# Descripcion: Run DockerOperator
# Version: 0.1

from airflow import DAG, utils
from airflow.operators.sensors import TimeDeltaSensor
from airflow.models import Variable
from datetime import datetime, timedelta, date, time
from airflow.operators.python_operator import PythonOperator

from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.contrib.kubernetes.pod import Resources
from airflow.contrib.kubernetes.volume import Volume
from airflow.contrib.kubernetes.volume_mount import VolumeMount
#from airflow.operators.docker_operator import DockerOperator


from airflow.operators.bash_operator import BashOperator
from airflow.operators.bcitools import BteqOperator
from airflow.operators.bcitools import FastLoadOperator
from airflow.operators.email_operator import EmailOperator
from airflow import configuration as conf
import bcitools.utils.functions as ba
#import bci.airflow.utils as ba

import logging
import os
import sys

from airflow.utils import timezone
import pendulum
from airflow.operators.dummy_operator import DummyOperator

reload(sys)

# INI CONFIG DAG
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
#teradata_credentials = Variable.get("td_credentials_prod", deserialize_json=True)
#docker_conf = Variable.get("docker_conf", deserialize_json=True)

#GMT = ba.getVarIfExists("GMT", 3)
#GMT = ba.getVarIfExists("GMT", 4)  # Obtenemos el ajuste de hora

#start = datetime(2021, 7, 15)  # ayer como start date
start = utils.dates.days_ago(50).astimezone(pendulum.timezone(Variable.get('TZ')))

#start = datetime.combine(date(start.year, start.month, start.day), time(0, 0))  # a las 0.00
default_args = {
    'owner': 'Analytics',
    'start_date': start,
    'email': ['hugo.pacheco@bci.cl'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 10,
    'retry_delay': timedelta(minutes=2),
    #'on_failure_callback': ba.slack_on_failure_callback,
    #'on_retry_callback': ba.slack_on_retry_callback,
}

# FIN CONFIG DAG

# INI CALENDARIO DAG

dag = DAG('406_Ejecutable_Modelo_AUM_v4', default_args=default_args, schedule_interval="0 14 18 * *")
#t0 = TimeDeltaSensor(task_id='Inicio_HHMM_PM', delta=timedelta(hours=18 + int(GMT), minutes=00), dag=dag)

t0 = DummyOperator(
    task_id='Inicio_14_00',
    dag=dag
)

dag_tasks = [t0]

# FIN CALENDARIO DAG

#SQL Operator
Copia_Tabla_Temporal_Final = BteqOperator(
        bteq='00_Copia_Tabla_Temporal_Final.sql',
        task_id='00_Copia_Tabla_Temporal_Final',
        conn_id='td_ttu_prod_empresas',
        pool='teradata-prod',
        depends_on_past=True,
        provide_context=True,
        xcom_push=True,
        xcom_all=True,
        dag=dag)

#Mail Operator
Ejecuta_Mail_Operator = EmailOperator(
    task_id='Mail_Fin',
    to=['hugo.pacheco@bci.cl'],
    subject='Carga Modelo Finalizada',
    html_content="Estimados, Finalizo la carga del Modelo Aum v1",
    #on_success_callback=ba.slack_on_success_callback,
    dag=dag)


# Define Modelo Inv
"""Ejecuta_py_inv = DockerOperator(
    docker_url=docker_conf.get('host'),
    image='ops/py-ejecutables:v1',
    task_id='Ejecuta_py_inv',
    xcom_push=True,
    xcom_all=True,
    pool='scoring_model_big',
    priority_weight=10,
    api_version=docker_conf.get('api_version'),
    wait_for_downstream=True,
    command='python Ejecutable+Modelo+AUM+Clientes.py',
    volumes=['/var/lib/docker/volumes/airflow19-analytics-personas-prod_airflow-dags-repo/_data/prod_analytcs-personas-20/40_Procesos_mensuales/406_Ejecutable_Modelos_Inversion_AUM/Modelos/:/opt/bci_utils/bciutils'],
    destroy_on_finish=True,
    dag=dag)"""

volume_mounts  = [VolumeMount('airflow-sharedfiles', mount_path='/opt/ruta_trabajo',sub_path=None, read_only=False),VolumeMount('airflow-files', mount_path='/data',sub_path=None, read_only=False)]
volumes  = [Volume(name='airflow-sharedfiles', configs={'persistentVolumeClaim': {'claimName': 'azfile-airflow-sharedfiles'}}),Volume(name='airflow-files', configs={'persistentVolumeClaim': {'claimName': 'azfile-airflow-tmpfiles'}})]
Ejecuta_py_inv = KubernetesPodOperator(
    namespace='ops-desa',
    image='bcidsrcnrarflw001.azurecr.io/bci/base-teradata:latest',
    task_id='406_Ejecutable_Modelo_Inversion',
    name='Ejecutable_Modelo_Inversion_406',
    xcom_push=True,
    xcom_all=True,
    pool='scoring_model_big',
    priority_weight=10,
    wait_for_downstream=True,
    cmds=["bash", "-c"],
    arguments=["python /opt/ruta_trabajo/406_Ejecutable_Modelos_Inversion_AUM/Ejecutable+Modelo+AUM+Clientes.py"],
    volumes=volumes,
    volume_mounts=volume_mounts,
    is_delete_operator_pod=False,
    dag=dag)
"""Ejecuta_py_inv = KubernetesPodOperator(
    namespace='ops-desa',
    image='bcidsrcnrarflw001.azurecr.io/bci/py-ejecutables:v1',
    task_id='406_Ejecutable_Modelo_Inversion',
    xcom_push=True,
    xcom_all=True,
    #executor_config={
    #    "KubernetesExecutor": {"request_memory": "5632Mi", "limit_memory": "5632Mi", "request_cpu": "3000m","limit_cpu": "3000m"}},
    pool='scoring_model_big',
    name='ejecutable_modelo_inversion',
    priority_weight=10,
    wait_for_downstream=True,
    cmds=["bash", "-c"],
    arguments=['python /files/shared/406_Ejecutable_Modelos_Inversion_AUM/Ejecutable+Modelo+AUM+Clientes.py'],
    #env_vars={
    #    'INPUT_PATH': '/data/Modelo_Correos/aggregated_mails.csv',
    #    'OUTPUT_PATH': '/data/Modelo_Correos/clean_mails.csv'
    #},
    volumes=volumes,
    volume_mounts=volume_mounts,
    is_delete_operator_pod=True,
    execution_timeout=timedelta(minutes=9),
    retries=3,
    dag=dag)"""

# Genera Bteqs
def define_bteq_op(bteq_file, bteq_name, bteq_params={}):
    """ Define operador BTEQ para ejecutar en contenedor"""
    return BteqOperator(
        bteq=os.path.join(queries_folder, os.path.basename(bteq_file)),
        task_id=bteq_name,
        conn_id='td_ttu_prod_empresas',
        pool='teradata-prod',
        depends_on_past=True,
        provide_context=True,
        xcom_push=True,
        xcom_all=True,
        dag=dag)

import glob

queries_folder = 'Tablon'
ext_file = '.sql'
bteq_files = [f for f in glob.glob('%s/*%s' % (os.path.join(__location__, queries_folder), ext_file)) if
              f.endswith(ext_file)]

for bteq_file in sorted(bteq_files):
    try:
        query_task_id = os.path.basename(bteq_file).split(ext_file)[0]
    except:
        query_task_id = str(uuid.uuid4())
        logging.info("Archivo con el nombre malo : " + bteq_file)
        pass
    dag_tasks.append(define_bteq_op(bteq_file, query_task_id))


copia_archivos = BashOperator(
    task_id='copia_archivos',
    bash_command='cp -a /usr/local/airflow/dags/repo/carpeta_desarrollo/Simulacion/406_Ejecutable_Modelos_Inversion_AUM/Modelos/. /files/shared/406_Ejecutable_Modelos_Inversion_AUM',
    dag=dag)    

# ORDEN DE EJECUCION 1
#t0

# Definiendo dependencias, se asume secuencialidad
for seq_pair in zip(dag_tasks, dag_tasks[1:]):
    seq_pair[0] >> seq_pair[1]

# ORDEN DE EJECUCION 2
seq_pair[1] >> copia_archivos >> Ejecuta_py_inv >> Copia_Tabla_Temporal_Final >> Ejecuta_Mail_Operator




