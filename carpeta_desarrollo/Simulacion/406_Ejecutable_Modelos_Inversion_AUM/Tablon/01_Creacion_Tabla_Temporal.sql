/*********************************************************************
**********************************************************************
** DSCRPCN: CREA TABLAS PARA LA CARGA DE MODELOS VIA AIRFLOW        **
**																	**
** AUTOR  : CCC                                                     **
** EMPRESA: BCI                                                     **
** FECHA  : 04/2019                                                 **
*********************************************************************/
/*********************************************************************
** MANTNCN:                                                         **
** AUTOR  :                                                         **
** FECHA  : SSAAMMDD                                                **
/*********************************************************************
** TABLA CREADA :		EDW_TEMPUSU.SG_MOD_AUM_CLI		         	**
**                                                              	**
**********************************************************************
*********************************************************************/
.SET SESSION CHARSET 'UTF8';
SELECT DATE, TIME;

INSERT INTO MKT_CRM_ANALYTICS_TB.R_MODELO_EJECUCION
SELECT CURRENT_TIMESTAMP
	,'INICIO'
	,'406_Ejecutable_Modelo_AUM_v1'
	,'Analytics_Persona';

/* **********************************************************************/
/* 			        SE EJECUTA LA CREACION DE TABLAS                    */
/* **********************************************************************/

DROP TABLE EDW_TEMPUSU.SG_MOD_AUM_CLI;
CREATE SET TABLE EDW_TEMPUSU.SG_MOD_AUM_CLI ,FALLBACK ,
    NO BEFORE JOURNAL,
    NO AFTER JOURNAL,
    CHECKSUM = DEFAULT,
    DEFAULT MERGEBLOCKRATIO
    (
RUT INTEGER,
FECHA_REF INTEGER,
MODELO_ID INTEGER,
VALOR INTEGER)
PRIMARY INDEX (FECHA_REF, RUT, MODELO_ID );

.IF ERRORCODE <> 0 THEN .QUIT 1;

SELECT DATE, TIME;
.quit 0;