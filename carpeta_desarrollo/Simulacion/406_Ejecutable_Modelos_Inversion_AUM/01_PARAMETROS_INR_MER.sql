
/***********************************************************************************************
** DESCRIPCION: PARAMETROS DE INICIALIZACION DEL PROCESO (FECHAS)							  **
** AUTOR: German Oviedo			                                                              **
** FECHA : Septiembre 2020 (documentado Noviembre 2020)                                       **
************************************************************************************************
** PARAMETROS UTILIZADOS                                                                      **
** FECHA PROCESO: FORMATO YYYYMM DESDE AIRFLOW                                                **
***********************************************************************************************/
/***********************************************************************************************
** TABLAS DE SALIDA :                                                                         **
**    MKT_TMP_INPUT_TB.T_{{ds_nodash}}_AP_VALOR_PARAMS                                             **
***********************************************************************************************/


.SET SESSION CHARSET 'UTF8';
SELECT DATE, TIME;


DROP TABLE MKT_TMP_INPUT_TB.T_{{ds_nodash}}_RM_PARAMETROS;
CREATE TABLE MKT_TMP_INPUT_TB.T_{{ds_nodash}}_RM_PARAMETROS
(
    TF_FECHA_EJEC 				DATE FORMAT 'YYYY-MM-DD',
    TC_FECHA_REF			 	CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
	TC_FECHA_REF_MENOS2			CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
	TE_MESES_ANTIGUEDAD_PROP	INTEGER,	
	TE_MESES_ANTIGUEDAD_SCORE	INTEGER,
	TF_FECHA_REF_DIA		 	DATE FORMAT 'YYYY-MM-DD',
	TE_FECHA_REF_MESES			INTEGER,
	TE_FECHA_REF_MESES_SBIF		INTEGER,
	TE_FECHA_REF_MESES_MEDIR	INTEGER

)
	PRIMARY INDEX (TF_FECHA_EJEC,TC_FECHA_REF);	
	
	.IF ERRORCODE <> 0 THEN .QUIT 1;	


INSERT INTO MKT_TMP_INPUT_TB.T_{{ds_nodash}}_RM_PARAMETROS
	SELECT
	CAST('{{ ds }}' AS DATE) + INTERVAL '1' MONTH as FECHA_EJEC
	,CAST(FECHA_EJEC AS DATE FORMAT 'YYYYMMDD')(CHAR(6))  AS FECHA_REF -- Esta es la fecha de salida que esta en las tablas finales 
	,cast(ADD_MONTHS( cast(cast(Fecha_Ref as char(6) ) as date format 'yyyymm' ) , -2) as date format 'yyyymm')(char(6)) as Fecha_Ref_menos2
	,12 AS MESES_ANTIGUEDAD_PROP
	,12 AS MESES_ANTIGUEDAD_SCORE
	,CAST (SUBSTR(TRIM(FECHA_REF),1,4)||'-'||SUBSTR(TRIM(FECHA_REF),5,2)||'-01'  AS DATE) AS FECHA_REF_DIA
	,floor(FECHA_REF/100)*12 + FECHA_REF MOD 100 AS FECHA_REF_MESES
	,(floor(FECHA_REF/100)*12 + FECHA_REF MOD 100)-2 AS FECHA_REF_MESES_SBIF
	,(floor(FECHA_REF/100)*12 + FECHA_REF MOD 100)-5 AS FECHA_REF_MESES_MEDIR;

       
   .IF ERRORCODE <> 0 THEN .QUIT 2;

   
COLLECT STATISTICS COLUMN (TF_FECHA_EJEC,TC_FECHA_REF) ON MKT_TMP_INPUT_TB.T_{{ds_nodash}}_RM_PARAMETROS;

	.IF ERRORCODE <> 0 THEN .QUIT 3;
 

SELECT DATE, TIME;
.QUIT 0;