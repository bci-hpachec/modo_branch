# -*- coding: utf-8 -*-
#primer paso
from airflow import DAG, utils
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.macros import ds_format
from airflow.models import Variable
from airflow.hooks.bcitools import TeradataHook
import bcitools.utils.functions as ba
import logging
import os
import sys
import glob
import pendulum

from random import randint
from datetime import datetime

def print_context(**kwargs):
    """Print the Airflow context and ds variable from the context."""
    print('Whatever you return gets printed in the logs')
    print(kwargs)
    #print(ds)
    return 'Whatever you return gets printed in the logs'

def _choose_best_model(ti):
    accuracies = ti.xcom_pull(key='model_accuracy', task_ids=[
        'training_model_A',
        'training_model_B',
        'training_model_C'
    ])
    best_accuracy = max(accuracies)
    if (best_accuracy > 8):
        return 'accurate'
    return 'inaccurate'


def _training_model(**kwargs):
    #return randint(1, 10)
    accuracy = randint(1, 10)
    print(f'model\'s accuracy: {accuracy}')
    kwargs['ti'].xcom_push(key='model_accuracy', value=accuracy)

#segundo paso
dag = DAG("DO_004_Modelo_Xcom", start_date=datetime(2021, 1, 1), schedule_interval="@daily", catchup=False)
#tercer paso

run_this = PythonOperator(
    task_id='print_the_context',
    provide_context=True,
    python_callable=print_context,
    dag=dag
)


training_model_A = PythonOperator(
    task_id="training_model_A",
    provide_context=True,
    python_callable=_training_model,
    dag=dag
)

training_model_B = PythonOperator(
    task_id="training_model_B",
    provide_context=True,
    python_callable=_training_model,
    dag=dag
)

training_model_C = PythonOperator(
    task_id="training_model_C",
    provide_context=True,
    python_callable=_training_model,
    dag=dag
)

choose_best_model = PythonOperator(
    task_id="choose_best_model",
    #op_kwargs={'ti':task_instance},
    provide_context=True,
    python_callable=_choose_best_model,
    dag=dag
)

accurate = BashOperator(
    task_id="accurate",
    bash_command="echo 'accurate'",
    dag=dag
)

inaccurate = BashOperator(
    task_id="inaccurate",
    bash_command="echo 'inaccurate'",
    dag=dag
)

#cuarto paso
run_this >> [training_model_A, training_model_B, training_model_C] >> choose_best_model >> [accurate, inaccurate]

