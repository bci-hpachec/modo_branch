/*********************************************************************
**********************************************************************
** DSCRPCN: CREA TABLAS PARA LA CARGA DE MODELOS VIA AIRFLOW        **
**																	**
** AUTOR  : EMR                                                     **
** EMPRESA: BCI                                                     **
** FECHA  : 01/2020                                                 **
*********************************************************************/
/*********************************************************************
** MANTNCN:                                                         **
** AUTOR  :                                                         **
** FECHA  : SSAAMMDD                                                **
/*********************************************************************
** TABLA CREADA :		EDW_TEMPUSU.MOD_TC       		    	    **
** COMENTARIO   :       SE OCUPA TABLON DE MODELOS PROCESO 217      **
**********************************************************************
*********************************************************************/
.SET SESSION CHARSET 'UTF8';
SELECT DATE, TIME;

--TRACKING DEL MODELO
INSERT INTO MKT_CRM_ANALYTICS_TB.R_MODELO_EJECUCION
SELECT CURRENT_TIMESTAMP
	,'INICIO'
	,'421_Ejecutable_Modelos_Tarjeta_1'
	,'Analytics_Persona';

/* **********************************************************************/
/* 			        SE EJECUTA LA CREACION DE TABLAS                    */
/* **********************************************************************/
DROP TABLE EDW_TEMPUSU.MOD_TC;
CREATE SET TABLE EDW_TEMPUSU.MOD_TC ,FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
        FECHA_REF INTEGER,
        PARTY_ID INTEGER,
        RUT INTEGER,
        PROB FLOAT,
        PROBA FLOAT,
        MODELO_ID INTEGER )
PRIMARY INDEX (FECHA_REF, RUT );
.IF ERRORCODE <> 0 THEN .QUIT 1;



.QUIT 0;