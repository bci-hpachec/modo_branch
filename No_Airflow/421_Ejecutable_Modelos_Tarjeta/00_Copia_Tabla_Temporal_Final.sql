/*********************************************************************
**********************************************************************
** DSCRPCN: CARGA DESDE TEMPORAL A TABLA FINAL                      **
** AUTOR  : EMR                                                     **
** EMPRESA: BCI                                                     **
** FECHA  : 01/2020                                                 **
*********************************************************************/
/*********************************************************************
** MANTNCN:                                                         **
** AUTOR  :                                                         **
** FECHA  : SSAAMMDD                                                **
/*********************************************************************
** ID_MODELOS CLIENTES  :   ////                          **

** TABLA ENTRADA        :   EDW_TEMPUSU.MOD_SEG                     **
**                                                                  **
** TABLA FINAL          :   MKT_CRM_ANALYTICS_TB.MP_SEG_PROB_HIST   **
**********************************************************************
*********************************************************************/
.SET SESSION CHARSET 'UTF8';
SELECT DATE, TIME;

/* **********************************************************************/
/* 			            CARGAMOS EN TABLA FINAL                         */
/* **********************************************************************/

DELETE FROM MKT_CRM_ANALYTICS_TB.MP_TC_PROB_HIST
WHERE MODELO_ID||FECHA_REF IN (SELECT MODELO_ID||FECHA_REF FROM EDW_TEMPUSU.MOD_TC);
.IF ERRORCODE <> 0 THEN .QUIT 1;

INSERT INTO MKT_CRM_ANALYTICS_TB.MP_TC_PROB_HIST
SELECT FECHA_REF, MODELO_ID, PARTY_ID, RUT, PROB, PROBA
FROM EDW_TEMPUSU.MOD_TC;
.IF ERRORCODE <> 0 THEN .QUIT 2;


DELETE FROM MKT_CRM_ANALYTICS_TB.MP_BCI_PROB_HIST
WHERE MODELO_ID||FECHA_REF IN (SELECT MODELO_ID||FECHA_REF FROM EDW_TEMPUSU.MOD_TC);
.IF ERRORCODE <> 0 THEN .QUIT 3;

INSERT INTO MKT_CRM_ANALYTICS_TB.MP_BCI_PROB_HIST
SELECT
A.RUT
,A.PARTY_ID
,A.FECHA_REF
,A.MODELO_ID
,A.PROBA
,0 AS RENTABILIDAD_ESPERADA
,0 AS TRAMO_ID1
,0 AS TRAMO_ID2
,0 AS TRAMO_ID3
,0 AS TARGET
,TRIM((A.FECHA_REF||'_1')) AS COD_EJECUCION
FROM EDW_TEMPUSU.MOD_TC A
WHERE A.MODELO_ID IN (26,27,28,29,30,31,32,33,34,35,36);
.IF ERRORCODE <> 0 THEN .QUIT 4;

--TRACKING DEL MODELO
INSERT INTO MKT_CRM_ANALYTICS_TB.R_MODELO_EJECUCION
SELECT CURRENT_TIMESTAMP
	,'TERMINO'
	,'421_Ejecutable_Modelos_Tarjeta_1'
	,'Analytics_Persona';

INSERT INTO MKT_CRM_ANALYTICS_TB.R_MODELOS_RESULTADOS
SELECT CURRENT_TIMESTAMP
	,'421_Ejecutable_Modelos_Tarjeta_1'
	,'Analytics_Persona'
	,'MKT_CRM_ANALYTICS_TB.MP_BCI_PROB_HIST'
	,'Mensual'
	,CAST(MAX(Fecha_Ref) AS VARCHAR(6))
	,COUNT(1) as N
FROM MKT_CRM_ANALYTICS_TB.MP_BCI_PROB_HIST
WHERE MODELO_ID IN (26,27,28,29,30,31,32,33,34,35,36);

SELECT DATE, TIME;
.QUIT 0;

