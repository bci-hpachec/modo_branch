#!/usr/bin/env python
# coding: utf-8

# In[1]:


import giraffez
import pandas as pd
import numpy as np
from xgboost import XGBClassifier
from sklearn.externals import joblib
from sklearn.base import TransformerMixin


def impute(X):
    return X.fillna(-99999999999)


class ColumnWatcher(TransformerMixin):
    def fit(self, X, y=None):
        self.columns = list(X.columns)
        return self

    def transform(self, X, y=None):
        return X


# In[3]:


td_config = {
    "username": "usr_an_common",
    "password": "anlc1812",
    "host": "dataware.bci.cl"}

# In[4]:

with giraffez.BulkExport("""
SELECT
party_id,
rut,
fecha_ref,
max_12m_disp_tdc_bci,
max_12m_marg_tdc_bci,
max_12m_marg_con_bci,
max_12m_disp_con_bci,
NUM_AVG_CARGO_CCT_3M,
max_12m_marg_glob_bci,
NUM_AVG_CARGO_CCT_6M,
NUM_AVG_CARGO_CCT_1M,
NUM_AVG_CARGO_CCT_3M_6M,
din_fuera_tdtc,
din_cons_tdtc,
MAX_NUM_CARGO_CCT_1M,
recencia_cierre_tc,
edad,
IND_FILTRO_RIESGO,
antiguedad_primer_contrato,
max_n_tc_vig_12m,
max_6m_cupo_nac_tc_nba,
max_6m_cupo_int_tc_nba,
med3mant_saldo_nac_tc_nba,
med3mant_saldo_int_tc_nba,
cupo_sbif_vs_renta,
renta,
FACT_TC_DEU_NAC_AVG_3M_6M,
max_12m_marg_ldc_bci,
max_12m_disp_ldc_bci,
MAX_ABONO_CCT_1M,
EVOL_AVG_SALDO_NAC_3M_6M,
NUM_AVG_CARGO_CCT_6M_12M,
AVG_CAMPANIAS_TC_EJECUTIVO_1M,
max_12m_cupo_nac_tc,
ult_ENDEUD_SBIF,
RECENCIA_BLOQUEO,
MTO_TRANSF_6M,
EVOL_NUM_ABONO_CCT_6M_12M,
ult_DeuLC_vs_cupo,
ant_primera_tc,
ant_ultima_tc,
edad,
EVOL_cupo_nac_tc_6M_12M,
NUM_MAX_COMPRAS_12M_NBA,
FACT_TC_PAGO_AVG_6M,
FACT_TC_MAX_12M,
NUM_MAX_COMPRAS_NAC_12M_NBA,
med6M_ENDEUD_SBIF,
AVG_SALDO_NAC_6M,
cupo_nac_disponible_var_extra,
MTO_AVG_COMPRAS_6M_NBA,
MTO_AVG_ABONO_CCT_1M,
MAX_NUM_AVANCES_12M,
ant_primera_cct,
EVOL_MTO_COMPRAS_3M_6M_NBA,
MTO_AVG_COMPRAS_3M_NBA,
NUM_MAX_COMPRAS_3M_NBA,
uso_cupo_nac_var_extra,
EVOL_NUM_COMPRAS_3M_6M_NBA,
NUM_AVG_TX_TC_3M_NBA,
MAX_SALDO_CUOTAS_1M,
RATIO_MTO_COMPRAS_1M_6M_NBA,
ult_DeuTC_vs_cupo,
EVOL_MTO_PAGOS_PAT_3M_6M,
EVOL_NUM_COMPRAS_6M_12M_NBA,
EVOL_MTO_COMPRAS_3M_6M_NBA,
EVOL_NUM_COMPRAS_NAC_6M_12M_NBA,
MAX_SALDO_NAC_12M,
MTO_MAX_COMPRAS_12M_NBA,
FACT_TC_CUO_AVG_6M,
ult_deuda_con_sbif,
MTO_TRANSF_A_BCI_6M,
MTO_AVG_ABONO_CCT_3M,
MTO_AVG_CARGO_CCT_3M,
NUM_AVG_INT_TC_AVANCE_TOT_12M,
DeuLC_vs_cupo_6M,
recencia_cierre_cons,
FACT_TC_CUO_MAX_12M,
MAX_SALDO_LC_1M,
NUM_AVG_INT_TC_AVANCE_TOT_3M,
FACT_TC_REV_AVG_3M,
MTO_MAX_AVANCES_CUOTAS_12M,
MTO_MAX_AVANCES_CUOTAS_3M,
FACT_TC_REV_AVG_1M,
NUM_MAX_AVANCES_CUOTAS_12M,
ant_ultimo_cons,
FACT_TC_REV_MAX_12M,
MTO_MAX_AVANCES_EFECTIVO_12M,
MAX_SALDO_LC_12M,
EVOL_NUM_AVANCES_6M_12M,
MTO_MAX_AVANCES_EFECTIVO_3M,
MTO_AVG_AVANCES_EFECTIVO_6M,
MTO_MAX_AVANCES_EFECTIVO_1M,
max_12m_deuda_con_sbif,
ind_cct,
ind_tc,
ind_tc_no_bloq_mor,
COUNT_TC,
NUM_AVG_COMPRAS_3M_NBA,
MTO_AVG_COMPRAS_3M_NBA,
NUM_AVG_AVANCES_6M,
antiguedad_ultima_tc
FROM 
Mkt_Crm_Analytics_Tb.mp_bci_tablon_analitico_AUX
WHERE BANCA IN ('PBP','PBU','PP','PRE','PM','PME','PMN','PMR','PEQ','PE') AND tipo_banca = 'BCI'""", **td_config) as export:
    data = pd.DataFrame.from_dict(export.to_dict())  # dejar backup

# AUMENTO USO SOBRE

# In[4]:

pipeline_aums= pd.read_pickle('AUM_USO_TC_A_12_vars.mod')

# In[5]:

data_aums= data[(data.ind_tc > 0.0) & (data.ind_tc_no_bloq_mor == 1)  & (data.mto_avg_compras_3m_nba >= 80000) & (data.num_max_compras_3m_nba > 0)  & (data.uso_cupo_nac_var_extra <= 0.75)  & (data.antiguedad_ultima_tc >= 6.0)].rename(columns = {'evol_mto_compras_3m_6m_nba':'EVOL_MTO_COMPRAS_3M_6M_NBA', 	'mto_avg_compras_3m_nba':'MTO_AVG_COMPRAS_3M_NBA', 	'num_max_compras_3m_nba':'NUM_MAX_COMPRAS_3M_NBA', 	'evol_num_compras_3m_6m_nba':'EVOL_NUM_COMPRAS_3M_6M_NBA', 	'num_avg_tx_tc_3m_nba':'NUM_AVG_TX_TC_3M_NBA', 	'max_saldo_cuotas_1m':'MAX_SALDO_CUOTAS_1M', 	'ratio_mto_compras_1m_6m_nba':'RATIO_MTO_COMPRAS_1M_6M_NBA', 	'ult_deutc_vs_cupo':'ult_DeuTC_vs_cupo', 	'evol_mto_pagos_pat_3m_6m':'EVOL_MTO_PAGOS_PAT_3M_6M', 	'fact_tc_pago_avg_6m':'FACT_TC_PAGO_AVG_6M'})

dataset_aums= data_aums.reindex(columns = ['EVOL_MTO_COMPRAS_3M_6M_NBA',	'MTO_AVG_COMPRAS_3M_NBA',	'NUM_MAX_COMPRAS_3M_NBA',	'uso_cupo_nac_var_extra',	'EVOL_NUM_COMPRAS_3M_6M_NBA',	'max_12m_cupo_nac_tc',	'NUM_AVG_TX_TC_3M_NBA',	'MAX_SALDO_CUOTAS_1M',	'RATIO_MTO_COMPRAS_1M_6M_NBA',	'ult_DeuTC_vs_cupo',	'EVOL_MTO_PAGOS_PAT_3M_6M',	'FACT_TC_PAGO_AVG_6M'])

# In[6]:

probs_aums= pd.DataFrame({"prob": pipeline_aums.predict_proba(dataset_aums)[:, 1]})

# In[7]:


ruts_aums= pd.DataFrame({"rut": data_aums["rut"], "party_id": data_aums["party_id"], "fecha_ref": data_aums["fecha_ref"]}).reset_index()

# In[8]:


tablon_aums= pd.merge(ruts_aums, probs_aums, right_index=True, left_index=True)

# In[9]:


tablon_aums["rut"] = tablon_aums["rut"].astype(int)

# In[46]:

tablon_aums["proba"] = tablon_aums.prob * 0.0280*(1-0.0497) / ((tablon_aums.prob * 0.0280*(1-0.0497))+((1 - tablon_aums.prob)*(1-0.0280)*0.0497))

# In[11]:


tablon_aums["modelo_id"] = 26

# In[47]:


# In[48]:

with giraffez.BulkLoad("EDW_TEMPUSU.MOD_TC", **td_config) as load:
    load.cleanup()
    load.columns = tablon_aums.columns.tolist()
    for row in tablon_aums.values.tolist():
        load.put([int(row[1]), int(row[2]), int(row[3]), row[4], row[5], int(row[6])])

# In[ ]:

# AUMENTO USO BAJO

# In[4]:

pipeline_aumb= pd.read_pickle('AUM_USO_TC_b_17_vars.mod')

# In[5]:

data_aumb= data[(data.ind_tc > 0.0) & (data.ind_tc_no_bloq_mor == 1) & (data.mto_avg_compras_3m_nba < 80000)  & (data.num_max_compras_3m_nba > 0.0) & (data.uso_cupo_nac_var_extra <= 0.75) & (data.antiguedad_ultima_tc >= 6.0)].rename(columns = {'num_max_compras_3m_nba':'NUM_MAX_COMPRAS_3M_NBA', 	'evol_num_compras_6m_12m_nba':'EVOL_NUM_COMPRAS_6M_12M_NBA', 	'evol_mto_compras_3m_6m_nba':'EVOL_MTO_COMPRAS_3M_6M_NBA', 	'mto_avg_compras_3m_nba':'MTO_AVG_COMPRAS_3M_NBA', 	'num_max_compras_nac_12m_nba':'NUM_MAX_COMPRAS_NAC_12M_NBA', 	'evol_num_compras_nac_6m_12m_nba':'EVOL_NUM_COMPRAS_NAC_6M_12M_NBA', 	'num_max_compras_12m_nba':'NUM_MAX_COMPRAS_12M_NBA', 	'fact_tc_pago_avg_6m':'FACT_TC_PAGO_AVG_6M', 	'max_saldo_nac_12m':'MAX_SALDO_NAC_12M', 	'mto_max_compras_12m_nba':'MTO_MAX_COMPRAS_12M_NBA', 	'fact_tc_cuo_avg_6m':'FACT_TC_CUO_AVG_6M', 	'mto_transf_a_bci_6m':'MTO_TRANSF_A_BCI_6M', 	'mto_avg_abono_cct_3m':'MTO_AVG_ABONO_CCT_3M', 	'mto_avg_cargo_cct_3m':'MTO_AVG_CARGO_CCT_3M'})

dataset_aumb= data_aumb.reindex(columns = ['NUM_MAX_COMPRAS_3M_NBA',	'EVOL_NUM_COMPRAS_6M_12M_NBA',	'EVOL_MTO_COMPRAS_3M_6M_NBA',	'MTO_AVG_COMPRAS_3M_NBA',	'NUM_MAX_COMPRAS_NAC_12M_NBA',	'EVOL_NUM_COMPRAS_NAC_6M_12M_NBA',	'NUM_MAX_COMPRAS_12M_NBA',	'FACT_TC_PAGO_AVG_6M',	'max_12m_cupo_nac_tc',	'MAX_SALDO_NAC_12M',	'MTO_MAX_COMPRAS_12M_NBA',	'FACT_TC_CUO_AVG_6M',	'uso_cupo_nac_var_extra',	'ult_deuda_con_sbif',	'MTO_TRANSF_A_BCI_6M',	'MTO_AVG_ABONO_CCT_3M',	'MTO_AVG_CARGO_CCT_3M'])

# In[6]:

probs_aumb= pd.DataFrame({"prob": pipeline_aumb.predict_proba(dataset_aumb)[:, 1]})

# In[7]:


ruts_aumb= pd.DataFrame({"rut": data_aumb["rut"], "party_id": data_aumb["party_id"], "fecha_ref": data_aumb["fecha_ref"]}).reset_index()

# In[8]:


tablon_aumb= pd.merge(ruts_aumb, probs_aumb, right_index=True, left_index=True)

# In[9]:


tablon_aumb["rut"] = tablon_aumb["rut"].astype(int)

# In[46]:

tablon_aumb["proba"] = tablon_aumb.prob

# In[11]:


tablon_aumb["modelo_id"] = 27

# In[47]:


# In[48]:

with giraffez.BulkLoad("EDW_TEMPUSU.MOD_TC", **td_config) as load:
    load.cleanup()
    load.columns = tablon_aumb.columns.tolist()
    for row in tablon_aumb.values.tolist():
        load.put([int(row[1]), int(row[2]), int(row[3]), row[4], row[5], int(row[6])])

# In[ ]:

# ACTIVACION SALDO NULO

# In[4]:

pipeline_actsn= pd.read_pickle('ACTIV_TC_a_10_vars.mod')

# In[5]:

data_actsn= data[(data.ind_tc > 0.0) & (data.ind_tc_no_bloq_mor == 1) & (data.avg_saldo_nac_6m.isnull() == True) & ((data.num_avg_tx_tc_3m_nba == 0.0) | (data.num_avg_tx_tc_3m_nba.isnull() == True)) & (data.antiguedad_ultima_tc >= 6.0)].rename(columns = {'ult_endeud_sbif':'ult_ENDEUD_SBIF', 	'recencia_bloqueo':'RECENCIA_BLOQUEO', 	'mto_transf_6m':'MTO_TRANSF_6M', 	'evol_num_abono_cct_6m_12m':'EVOL_NUM_ABONO_CCT_6M_12M', 	'ult_deulc_vs_cupo':'ult_DeuLC_vs_cupo', 	'evol_cupo_nac_tc_6m_12m':'EVOL_cupo_nac_tc_6M_12M'})

dataset_actsn= data_actsn.reindex(columns = ['max_12m_cupo_nac_tc',	'ult_ENDEUD_SBIF',	'RECENCIA_BLOQUEO',	'MTO_TRANSF_6M',	'EVOL_NUM_ABONO_CCT_6M_12M',	'ult_DeuLC_vs_cupo',	'ant_primera_tc',	'ant_ultima_tc',	'edad', 'EVOL_cupo_nac_tc_6M_12M'])

# In[6]:

probs_actsn= pd.DataFrame({"prob": pipeline_actsn.predict_proba(dataset_actsn)[:, 1]})

# In[7]:


ruts_actsn= pd.DataFrame({"rut": data_actsn["rut"], "party_id": data_actsn["party_id"], "fecha_ref": data_actsn["fecha_ref"]}).reset_index()

# In[8]:


tablon_actsn= pd.merge(ruts_actsn, probs_actsn, right_index=True, left_index=True)

# In[9]:


tablon_actsn["rut"] = tablon_actsn["rut"].astype(int)

# In[46]:

tablon_actsn["proba"] = tablon_actsn.prob * 0.0168*(1-0.0524) / ((tablon_actsn.prob * 0.0168*(1-0.0524))+((1 - tablon_actsn.prob)*(1-0.0168)*0.0524))

# In[11]:


tablon_actsn["modelo_id"] = 28

# In[47]:


# In[48]:

with giraffez.BulkLoad("EDW_TEMPUSU.MOD_TC", **td_config) as load:
    load.cleanup()
    load.columns = tablon_actsn.columns.tolist()
    for row in tablon_actsn.values.tolist():
        load.put([int(row[1]), int(row[2]), int(row[3]), row[4], row[5], int(row[6])])

# In[ ]:

# ACTIVACION SALDO NO NULO

# In[4]:

pipeline_actsnn= pd.read_pickle('ACTIV_TC_b_13_vars.mod')

# In[5]:

data_actsnn= data[(data.ind_tc > 0.0) & (data.ind_tc_no_bloq_mor == 1) & (data.antiguedad_ultima_tc >= 6.0) & ((data.num_avg_tx_tc_3m_nba == 0.0) | (data.num_avg_tx_tc_3m_nba.isnull() == True)) & (data.avg_saldo_nac_6m.notnull() == True)].rename(columns = {'num_max_compras_12m_nba':'NUM_MAX_COMPRAS_12M_NBA', 	'num_avg_cargo_cct_1m':'NUM_AVG_CARGO_CCT_1M', 	'fact_tc_pago_avg_6m':'FACT_TC_PAGO_AVG_6M', 	'fact_tc_max_12m':'FACT_TC_MAX_12M', 	'num_max_compras_nac_12m_nba':'NUM_MAX_COMPRAS_NAC_12M_NBA', 	'med6m_endeud_sbif':'med6M_ENDEUD_SBIF', 	'avg_saldo_nac_6m':'AVG_SALDO_NAC_6M', 	'mto_avg_compras_6m_nba':'MTO_AVG_COMPRAS_6M_NBA', 	'mto_avg_abono_cct_1m':'MTO_AVG_ABONO_CCT_1M', 	'max_num_avances_12m':'MAX_NUM_AVANCES_12M'})

dataset_actsnn= data_actsnn.reindex(columns = ['NUM_MAX_COMPRAS_12M_NBA',	'NUM_AVG_CARGO_CCT_1M',	'edad',	'FACT_TC_PAGO_AVG_6M',	'FACT_TC_MAX_12M',	'NUM_MAX_COMPRAS_NAC_12M_NBA',	'med6M_ENDEUD_SBIF',	'AVG_SALDO_NAC_6M',	'cupo_nac_disponible_var_extra',	'MTO_AVG_COMPRAS_6M_NBA',	'MTO_AVG_ABONO_CCT_1M',	'MAX_NUM_AVANCES_12M',	'ant_primera_cct'])

# In[6]:

probs_actsnn= pd.DataFrame({"prob": pipeline_actsnn.predict_proba(dataset_actsnn)[:, 1]})

# In[7]:


ruts_actsnn= pd.DataFrame({"rut": data_actsnn["rut"], "party_id": data_actsnn["party_id"], "fecha_ref": data_actsnn["fecha_ref"]}).reset_index()

# In[8]:


tablon_actsnn= pd.merge(ruts_actsnn, probs_actsnn, right_index=True, left_index=True)

# In[9]:


tablon_actsnn["rut"] = tablon_actsnn["rut"].astype(int)

# In[46]:

tablon_actsnn["proba"] = tablon_actsnn.prob

# In[11]:


tablon_actsnn["modelo_id"] = 29

# In[47]:


# In[48]:

with giraffez.BulkLoad("EDW_TEMPUSU.MOD_TC", **td_config) as load:
    load.cleanup()
    load.columns = tablon_actsnn.columns.tolist()
    for row in tablon_actsnn.values.tolist():
        load.put([int(row[1]), int(row[2]), int(row[3]), row[4], row[5], int(row[6])])

# In[ ]:

# AVANCE CUOTAS NO AVANCEROS

# In[4]:

pipeline_avcna= pd.read_pickle('AVANCE_CUOTAS_a_13_vars.mod')

# In[5]:

data_avcna= data[(data.ind_tc > 0.0) & (data.ind_tc_no_bloq_mor == 1) & ((data.num_avg_avances_6m  == 0.0) | (data.num_avg_avances_6m.isnull() == True))].rename(columns = {'num_avg_int_tc_avance_tot_12m':'NUM_AVG_INT_TC_AVANCE_TOT_12M', 	'med6m_endeud_sbif':'med6M_ENDEUD_SBIF', 	'ult_endeud_sbif':'ult_ENDEUD_SBIF', 	'deulc_vs_cupo_6m':'DeuLC_vs_cupo_6M', 	'fact_tc_cuo_max_12m':'FACT_TC_CUO_MAX_12M', 	'max_num_avances_12m':'MAX_NUM_AVANCES_12M', 	'max_saldo_lc_1m':'MAX_SALDO_LC_1M', 	'num_avg_int_tc_avance_tot_3m':'NUM_AVG_INT_TC_AVANCE_TOT_3M', 	'fact_tc_rev_avg_3m':'FACT_TC_REV_AVG_3M'})

dataset_avcna= data_avcna.reindex(columns = ['cupo_nac_disponible_var_extra',	'max_12m_cupo_nac_tc',	'NUM_AVG_INT_TC_AVANCE_TOT_12M',	'med6M_ENDEUD_SBIF',	'ult_ENDEUD_SBIF',	'DeuLC_vs_cupo_6M',	'ant_ultima_tc',	'recencia_cierre_cons',	'FACT_TC_CUO_MAX_12M',	'MAX_NUM_AVANCES_12M',	'MAX_SALDO_LC_1M',	'NUM_AVG_INT_TC_AVANCE_TOT_3M',	'FACT_TC_REV_AVG_3M'])

# In[6]:

probs_avcna= pd.DataFrame({"prob": pipeline_avcna.predict_proba(dataset_avcna)[:, 1]})

# In[7]:


ruts_avcna= pd.DataFrame({"rut": data_avcna["rut"], "party_id": data_avcna["party_id"], "fecha_ref": data_avcna["fecha_ref"]}).reset_index()

# In[8]:


tablon_avcna= pd.merge(ruts_avcna, probs_avcna, right_index=True, left_index=True)

# In[9]:


tablon_avcna["rut"] = tablon_avcna["rut"].astype(int)

# In[46]:

tablon_avcna["proba"] = tablon_avcna.prob * 0.0091*(1-0.0575) / ((tablon_avcna.prob * 0.0091*(1-0.0575))+((1 - tablon_avcna.prob)*(1-0.0091)*0.0575))

# In[11]:


tablon_avcna["modelo_id"] = 30

# In[47]:


# In[48]:

with giraffez.BulkLoad("EDW_TEMPUSU.MOD_TC", **td_config) as load:
    load.cleanup()
    load.columns = tablon_avcna.columns.tolist()
    for row in tablon_avcna.values.tolist():
        load.put([int(row[1]), int(row[2]), int(row[3]), row[4], row[5], int(row[6])])

# In[ ]:

# AVANCE CUOTAS AVANCEROS

# In[4]:

pipeline_avca= pd.read_pickle('AVANCE_CUOTAS_b_12_vars.mod')

# In[5]:

data_avca= data[(data.ind_tc > 0.0) & (data.ind_tc_no_bloq_mor == 1) & (data.num_avg_avances_6m > 0.0)].rename(columns = {'mto_max_avances_cuotas_12m':'MTO_MAX_AVANCES_CUOTAS_12M', 	'fact_tc_cuo_max_12m':'FACT_TC_CUO_MAX_12M', 	'med6m_endeud_sbif':'med6M_ENDEUD_SBIF', 	'mto_max_avances_cuotas_3m':'MTO_MAX_AVANCES_CUOTAS_3M', 	'fact_tc_pago_avg_6m':'FACT_TC_PAGO_AVG_6M', 	'fact_tc_rev_avg_1m':'FACT_TC_REV_AVG_1M', 	'mto_avg_cargo_cct_3m':'MTO_AVG_CARGO_CCT_3M', 	'ult_endeud_sbif':'ult_ENDEUD_SBIF', 	'num_max_avances_cuotas_12m':'NUM_MAX_AVANCES_CUOTAS_12M'})

dataset_avca= data_avca.reindex(columns = ['max_12m_cupo_nac_tc',	'MTO_MAX_AVANCES_CUOTAS_12M',	'cupo_nac_disponible_var_extra',	'FACT_TC_CUO_MAX_12M',	'med6M_ENDEUD_SBIF',	'MTO_MAX_AVANCES_CUOTAS_3M',	'FACT_TC_PAGO_AVG_6M',	'FACT_TC_REV_AVG_1M',	'MTO_AVG_CARGO_CCT_3M',	'ult_ENDEUD_SBIF',	'NUM_MAX_AVANCES_CUOTAS_12M',	'ant_ultimo_cons'])

# In[6]:

probs_avca= pd.DataFrame({"prob": pipeline_avca.predict_proba(dataset_avca)[:, 1]})

# In[7]:


ruts_avca= pd.DataFrame({"rut": data_avca["rut"], "party_id": data_avca["party_id"], "fecha_ref": data_avca["fecha_ref"]}).reset_index()

# In[8]:


tablon_avca= pd.merge(ruts_avca, probs_avca, right_index=True, left_index=True)

# In[9]:


tablon_avca["rut"] = tablon_avca["rut"].astype(int)

# In[46]:

tablon_avca["proba"] = tablon_avca.prob

# In[11]:


tablon_avca["modelo_id"] = 31

# In[47]:


# In[48]:

with giraffez.BulkLoad("EDW_TEMPUSU.MOD_TC", **td_config) as load:
    load.cleanup()
    load.columns = tablon_avca.columns.tolist()
    for row in tablon_avca.values.tolist():
        load.put([int(row[1]), int(row[2]), int(row[3]), row[4], row[5], int(row[6])])

# In[ ]:

# AVANCE EFECTIVO NO AVANCEROS

# In[4]:

pipeline_avena= pd.read_pickle('AVANCE_EFECTIVO_a_12_vars.mod')

# In[5]:

data_avena= data[(data.ind_tc > 0.0) & (data.ind_tc_no_bloq_mor == 1) & ((data.num_avg_avances_6m == 0.0) | (data.num_avg_avances_6m.isnull() == True))].rename(columns = {'fact_tc_rev_max_12m':'FACT_TC_REV_MAX_12M', 	'mto_max_avances_efectivo_12m':'MTO_MAX_AVANCES_EFECTIVO_12M', 	'max_saldo_lc_12m':'MAX_SALDO_LC_12M', 	'med6m_endeud_sbif':'med6M_ENDEUD_SBIF', 	'evol_num_avances_6m_12m':'EVOL_NUM_AVANCES_6M_12M', 	'ult_deulc_vs_cupo':'ult_DeuLC_vs_cupo', 	'mto_transf_6m':'MTO_TRANSF_6M', 	'deulc_vs_cupo_6m':'DeuLC_vs_cupo_6M'})

dataset_avena= data_avena.reindex(columns = ['FACT_TC_REV_MAX_12M',	'cupo_nac_disponible_var_extra',	'MTO_MAX_AVANCES_EFECTIVO_12M',	'MAX_SALDO_LC_12M',	'med6M_ENDEUD_SBIF',	'EVOL_NUM_AVANCES_6M_12M',	'max_12m_cupo_nac_tc',	'ult_deuda_con_sbif',	'ult_DeuLC_vs_cupo',	'ant_ultima_tc',	'MTO_TRANSF_6M',	'DeuLC_vs_cupo_6M'])

# In[6]:

probs_avena= pd.DataFrame({"prob": pipeline_avena.predict_proba(dataset_avena)[:, 1]})

# In[7]:


ruts_avena= pd.DataFrame({"rut": data_avena["rut"], "party_id": data_avena["party_id"], "fecha_ref": data_avena["fecha_ref"]}).reset_index()

# In[8]:


tablon_avena= pd.merge(ruts_avena, probs_avena, right_index=True, left_index=True)

# In[9]:


tablon_avena["rut"] = tablon_avena["rut"].astype(int)

# In[46]:

tablon_avena["proba"] = tablon_avena.prob * 0.0051*(1-0.0503) / ((tablon_avena.prob * 0.0051*(1-0.0503))+((1 - tablon_avena.prob)*(1-0.0051)*0.0503))

# In[11]:


tablon_avena["modelo_id"] = 32

# In[47]:


# In[48]:

with giraffez.BulkLoad("EDW_TEMPUSU.MOD_TC", **td_config) as load:
    load.cleanup()
    load.columns = tablon_avena.columns.tolist()
    for row in tablon_avena.values.tolist():
        load.put([int(row[1]), int(row[2]), int(row[3]), row[4], row[5], int(row[6])])

# In[ ]:

# AVANCE EFECTIVO AVANCEROS

# In[4]:

pipeline_avea= pd.read_pickle('AVANCE_EFECTIVO_b_13_vars.mod')

# In[5]:

data_avea= data[(data.ind_tc > 0.0) & (data.ind_tc_no_bloq_mor == 1) & (data.num_avg_avances_6m > 0.0)].rename(columns = {'mto_transf_6m':'MTO_TRANSF_6M', 	'mto_max_avances_efectivo_12m':'MTO_MAX_AVANCES_EFECTIVO_12M', 	'mto_max_avances_efectivo_3m':'MTO_MAX_AVANCES_EFECTIVO_3M', 	'mto_avg_avances_efectivo_6m':'MTO_AVG_AVANCES_EFECTIVO_6M', 	'mto_max_avances_efectivo_1m':'MTO_MAX_AVANCES_EFECTIVO_1M', 	'mto_avg_abono_cct_3m':'MTO_AVG_ABONO_CCT_3M', 	'mto_avg_cargo_cct_3m':'MTO_AVG_CARGO_CCT_3M', 	'fact_tc_rev_max_12m':'FACT_TC_REV_MAX_12M', 	'fact_tc_pago_avg_6m':'FACT_TC_PAGO_AVG_6M'})

dataset_avea= data_avea.reindex(columns = ['MTO_TRANSF_6M',	'max_12m_cupo_nac_tc',	'MTO_MAX_AVANCES_EFECTIVO_12M',	'MTO_MAX_AVANCES_EFECTIVO_3M',	'MTO_AVG_AVANCES_EFECTIVO_6M',	'MTO_MAX_AVANCES_EFECTIVO_1M',	'cupo_nac_disponible_var_extra',	'MTO_AVG_ABONO_CCT_3M',	'MTO_AVG_CARGO_CCT_3M',	'ult_deuda_con_sbif',	'max_12m_deuda_con_sbif',	'FACT_TC_REV_MAX_12M',	'FACT_TC_PAGO_AVG_6M'])
# In[6]:

probs_avea= pd.DataFrame({"prob": pipeline_avea.predict_proba(dataset_avea)[:, 1]})

# In[7]:


ruts_avea= pd.DataFrame({"rut": data_avea["rut"], "party_id": data_avea["party_id"], "fecha_ref": data_avea["fecha_ref"]}).reset_index()

# In[8]:


tablon_avea= pd.merge(ruts_avea, probs_avea, right_index=True, left_index=True)

# In[9]:


tablon_avea["rut"] = tablon_avea["rut"].astype(int)

# In[46]:

tablon_avea["proba"] = tablon_avea.prob

# In[11]:


tablon_avea["modelo_id"] = 33

# In[47]:


# In[48]:

with giraffez.BulkLoad("EDW_TEMPUSU.MOD_TC", **td_config) as load:
    load.cleanup()
    load.columns = tablon_avea.columns.tolist()
    for row in tablon_avea.values.tolist():
        load.put([int(row[1]), int(row[2]), int(row[3]), row[4], row[5], int(row[6])])

# In[ ]:

# VENTA TC SIN TC ANTERIOR

# In[4]:

pipeline_vtcs= pd.read_pickle('01_Venta_Sin_TC_16_vars.mod')

# In[5]:& (data.ind_tc == 0)

data_vtcs= data[(data.ind_cct > 0.0) & (data.ant_primera_cct >= 0.5) & ((data.count_tc == 0)|(data.ind_tc == 0))].rename(columns = {'num_avg_cargo_cct_3m':'NUM_AVG_CARGO_CCT_3M', 'num_avg_cargo_cct_6m':'NUM_AVG_CARGO_CCT_6M', 	'num_avg_cargo_cct_1m':'NUM_AVG_CARGO_CCT_1M', 	'num_avg_cargo_cct_3m_6m':'NUM_AVG_CARGO_CCT_3M_6M', 'max_num_cargo_cct_1m':'MAX_NUM_CARGO_CCT_1M', 'ind_filtro_riesgo':'IND_FILTRO_RIESGO'})

dataset_vtcs= data_vtcs.reindex(columns = ['max_12m_disp_tdc_bci',	'max_12m_marg_tdc_bci',	'max_12m_marg_con_bci',	'max_12m_disp_con_bci',	'NUM_AVG_CARGO_CCT_3M',	'max_12m_marg_glob_bci',	'NUM_AVG_CARGO_CCT_6M',	'NUM_AVG_CARGO_CCT_1M',	'NUM_AVG_CARGO_CCT_3M_6M',	'din_fuera_tdtc',	'din_cons_tdtc',	'MAX_NUM_CARGO_CCT_1M',	'recencia_cierre_tc',	'edad',	'IND_FILTRO_RIESGO',	'antiguedad_primer_contrato'])

# In[6]:

probs_vtcs= pd.DataFrame({"prob": pipeline_vtcs.predict_proba(dataset_vtcs)[:, 1]})

# In[7]:


ruts_vtcs= pd.DataFrame({"rut": data_vtcs["rut"], "party_id": data_vtcs["party_id"], "fecha_ref": data_vtcs["fecha_ref"]}).reset_index()

# In[8]:


tablon_vtcs= pd.merge(ruts_vtcs, probs_vtcs, right_index=True, left_index=True)

# In[9]:


tablon_vtcs["rut"] = tablon_vtcs["rut"].astype(int)

# In[46]:

tablon_vtcs["proba"] = tablon_vtcs.prob * 0.0047*(1-0.05009) / ((tablon_vtcs.prob * 0.0047*(1-0.05009))+((1 - tablon_vtcs.prob)*(1-0.0047)*0.05009))

# In[11]:


tablon_vtcs["modelo_id"] = 34

# In[47]:


# In[48]:

with giraffez.BulkLoad("EDW_TEMPUSU.MOD_TC", **td_config) as load:
    load.cleanup()
    load.columns = tablon_vtcs.columns.tolist()
    for row in tablon_vtcs.values.tolist():
        load.put([int(row[1]), int(row[2]), int(row[3]), row[4], row[5], int(row[6])])

# In[ ]:

# VENTA TC CON TC ANTERIOR SIN ACTIVIDAD

# In[4]:

pipeline_vtccs= pd.read_pickle('02_Venta_Con_TC_Previa_SAP_14_vars.mod')

# In[5]:

data_vtccs= data[(data.ind_cct >= 0.0) & (data.ant_primera_cct >= 0.5) & (data.count_tc == 1) & ((data.num_avg_compras_3m_nba == 0) | (data.num_avg_compras_3m_nba.isnull() == True))].rename(columns = {'num_avg_cargo_cct_6m':'NUM_AVG_CARGO_CCT_6M', 'num_avg_cargo_cct_3m':'NUM_AVG_CARGO_CCT_3M'})

dataset_vtccs= data_vtccs.reindex(columns = ['max_12m_disp_tdc_bci',	'max_12m_marg_tdc_bci',	'max_12m_marg_con_bci',	'max_12m_disp_con_bci',	'NUM_AVG_CARGO_CCT_6M',	'NUM_AVG_CARGO_CCT_3M',	'max_n_tc_vig_12m',	'antiguedad_primer_contrato',	'max_6m_cupo_nac_tc_nba',	'max_6m_cupo_int_tc_nba',	'med3mant_saldo_nac_tc_nba',	'med3mant_saldo_int_tc_nba',	'cupo_sbif_vs_renta',	'renta'])

# In[6]:

probs_vtccs= pd.DataFrame({"prob": pipeline_vtccs.predict_proba(dataset_vtccs)[:, 1]})

# In[7]:


ruts_vtccs= pd.DataFrame({"rut": data_vtccs["rut"], "party_id": data_vtccs["party_id"], "fecha_ref": data_vtccs["fecha_ref"]}).reset_index()

# In[8]:


tablon_vtccs= pd.merge(ruts_vtccs, probs_vtccs, right_index=True, left_index=True)

# In[9]:


tablon_vtccs["rut"] = tablon_vtccs["rut"].astype(int)

# In[46]:

tablon_vtccs["proba"] = tablon_vtccs.prob * 0.0023*(1-0.05002) / ((tablon_vtccs.prob * 0.0023*(1-0.05002))+((1 - tablon_vtccs.prob)*(1-0.0023)*0.05002))

# In[11]:


tablon_vtccs["modelo_id"] = 35

# In[47]:


# In[48]:

with giraffez.BulkLoad("EDW_TEMPUSU.MOD_TC", **td_config) as load:
    load.cleanup()
    load.columns = tablon_vtccs.columns.tolist()
    for row in tablon_vtccs.values.tolist():
        load.put([int(row[1]), int(row[2]), int(row[3]), row[4], row[5], int(row[6])])

# In[ ]:

# VENTA TC CON TC ANTERIOR CON ACTIVIDAD

# In[4]:

pipeline_vtccc= pd.read_pickle('03_Venta_TC_STP_CA_18_vars.mod')

# In[5]:

data_vtccc= data[(data.ind_cct >= 1) & (data.ant_primera_cct >= 0.5) & (data.count_tc == 1) & (data.num_avg_compras_3m_nba > 0.0)].rename(columns = {'fact_tc_deu_nac_avg_3m_6m':'FACT_TC_DEU_NAC_AVG_3M_6M', 'max_abono_cct_1m':'MAX_ABONO_CCT_1M', 	'ind_filtro_riesgo':'IND_FILTRO_RIESGO', 	'evol_avg_saldo_nac_3m_6m':'EVOL_AVG_SALDO_NAC_3M_6M', 'num_avg_cargo_cct_6m_12m':'NUM_AVG_CARGO_CCT_6M_12M', 	'avg_campanias_tc_ejecutivo_1m':'AVG_CAMPANIAS_TC_EJECUTIVO_1M'})

dataset_vtccc= data_vtccc.reindex(columns = ['FACT_TC_DEU_NAC_AVG_3M_6M',	'max_12m_disp_tdc_bci',	'med3mant_saldo_nac_tc_nba',	'med3mant_saldo_int_tc_nba',	'max_n_tc_vig_12m',	'max_12m_marg_ldc_bci',	'max_12m_marg_tdc_bci',	'max_12m_disp_ldc_bci',	'MAX_ABONO_CCT_1M',	'IND_FILTRO_RIESGO',	'EVOL_AVG_SALDO_NAC_3M_6M',	'renta',	'max_6m_cupo_int_tc_nba',	'max_6m_cupo_nac_tc_nba',	'edad',	'antiguedad_primer_contrato',	'NUM_AVG_CARGO_CCT_6M_12M',	'AVG_CAMPANIAS_TC_EJECUTIVO_1M'])

# In[6]:

probs_vtccc= pd.DataFrame({"prob": pipeline_vtccc.predict_proba(dataset_vtccc)[:, 1]})

# In[7]:


ruts_vtccc= pd.DataFrame({"rut": data_vtccc["rut"], "party_id": data_vtccc["party_id"], "fecha_ref": data_vtccc["fecha_ref"]}).reset_index()

# In[8]:


tablon_vtccc= pd.merge(ruts_vtccc, probs_vtccc, right_index=True, left_index=True)

# In[9]:


tablon_vtccc["rut"] = tablon_vtccc["rut"].astype(int)

# In[46]:

tablon_vtccc["proba"] = tablon_vtccc.prob * 0.0044*(1-0.049985) / ((tablon_vtccc.prob * 0.0044*(1-0.049985))+((1 - tablon_vtccc.prob)*(1-0.0044)*0.049985))

# In[11]:


tablon_vtccc["modelo_id"] = 36

# In[47]:


# In[48]:

with giraffez.BulkLoad("EDW_TEMPUSU.MOD_TC", **td_config) as load:
    load.cleanup()
    load.columns = tablon_vtccc.columns.tolist()
    for row in tablon_vtccc.values.tolist():
        load.put([int(row[1]), int(row[2]), int(row[3]), row[4], row[5], int(row[6])])

# In[ ]: