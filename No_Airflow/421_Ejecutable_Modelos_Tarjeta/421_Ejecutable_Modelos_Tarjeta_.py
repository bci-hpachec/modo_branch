# -*- coding: utf-8 -*-
import sys
from importlib import reload

reload(sys)
#sys.setdefaultencoding('utf-8')

from airflow import DAG, utils
from airflow.operators.sensors import TimeDeltaSensor
from airflow.operators.sensors import ExternalTaskSensor
from airflow.models import Variable
from datetime import datetime, timedelta, date, time
from airflow.operators.python_operator import PythonOperator
#from airflow.operators.docker_operator import DockerOperator
from airflow.operators.docker import DockerOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.bcitools import BteqOperator
from airflow.operators.email_operator import EmailOperator
from airflow.operators.dummy_operator import DummyOperator
import bci.airflow.utils as ba
import logging
import os
import sys

from airflow.utils import timezone
import pendulum

reload(sys)

# INI CONFIG DAG
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
teradata_credentials = Variable.get("td_credentials_prod", deserialize_json=True)
docker_conf = Variable.get("docker_conf", deserialize_json=True)
GMT = ba.getVarIfExists("GMT", 3)

#start = datetime(2020, 01, 15)  # ayer como start date
start = utils.dates.days_ago(3).astimezone(pendulum.timezone(Variable.get('TZ')))
start = datetime.combine(date(start.year, start.month, start.day), time(0, 0))  # a las 0.00
default_args = {
    'owner': 'Analytics',
    'start_date': start,
    'email': ['eduardo.merlo@bci.cl','camilo.carrascoc@bci.cl','marcos.reiman@bci.cl'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 10,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': ba.slack_on_failure_callback,
    'on_retry_callback': ba.slack_on_retry_callback,
}

# FIN CONFIG DAG

# INI CALENDARIO DAG

dag = DAG('421_Ejecutable_Modelos_Tarjeta_1', default_args=default_args, schedule_interval="0 0 18 * *")


t0 = ExternalTaskSensor(
    task_id='waiting_217_Masivo_Mensual_Tablon_Analitico_BCI',
    external_dag_id='217_Masivo_Mensual_Tablon_Analitico_BCI',
    external_task_id='Ejecuta_Mail_Operator',
    allowed_states=['success'],
    execution_delta=None,
    execution_date_fn=None,
    dag=dag)

t1 = TimeDeltaSensor(task_id='Espera_14_30_AM', delta=timedelta(hours=14+ int(GMT), minutes= 30), dag=dag)

# FIN CALENDARIO DAG

# Dummy Operator
dummy_espera = DummyOperator(
    task_id='Espera_Inicio',
    dag=dag
)
dag_tasks = dummy_espera

#SQL Operator 1
Copia_Tabla_Temporal_Final = BteqOperator(
        bteq='00_Copia_Tabla_Temporal_Final.sql',
        task_id='00_Copia_Tabla_Temporal_Final',
        conn_id='Teradata-Analitics',
        pool='teradata-prod',
        depends_on_past=True,
        provide_context=True,
        xcom_push=True,
        xcom_all=True,
        dag=dag)

#SQL Operator 2
Copia_Tabla_Temporal = BteqOperator(
        bteq='Tablon/01_Creacion_Tabla_Temporal.sql',
        task_id='01_Creacion_Tabla_Temporal',
        conn_id='Teradata-Analitics',
        pool='teradata-prod',
        depends_on_past=True,
        provide_context=True,
        xcom_push=True,
        xcom_all=True,
        dag=dag)

#Mail Operator
Ejecuta_Mail_Operator = EmailOperator(
    task_id='Mail_Fin',
    to=['laura.meneses@bci.cl', 'eduardo.merlo@corporacion.bci.cl','camilo.carrascoc@corporacion.bci.cl','marcos.reiman@corporacion.bci.cl'],
    subject='Carga Finalizada - Modelos Tarjeta',
    html_content="Estimados, Ejecucion de Modelos correcta",
    on_success_callback=ba.slack_on_success_callback,
    dag=dag)

# Define Modelo
Ejecuta_py_tc = DockerOperator(
    docker_url=docker_conf.get('host'),
    image='ops/py-ejecutables:v1',
    task_id='Ejecuta_py_tc',
    xcom_push=True,
    xcom_all=True,
    pool='scoring_model_big',
    priority_weight=10,
    api_version=docker_conf.get('api_version'),
    wait_for_downstream=True,
    command='python Ejecutable.py',
    volumes=['/var/lib/docker/volumes/airflow19-analytics-personas-prod_airflow-dags-repo/_data/prod_analytcs-personas-20/40_Procesos_mensuales/421_Ejecutable_Modelos_Tarjeta/Modelos/:/opt/bci_utils/bciutils'],
    destroy_on_finish=True,
    dag=dag)

# ORDEN DE EJECUCION 1
t0 >> dummy_espera
t1 >> dummy_espera

# ORDEN DE EJECUCION 2
dummy_espera >> Copia_Tabla_Temporal >> Ejecuta_py_tc >> Copia_Tabla_Temporal_Final >>Ejecuta_Mail_Operator




