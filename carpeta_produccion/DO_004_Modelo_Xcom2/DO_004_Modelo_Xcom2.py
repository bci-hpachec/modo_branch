#primer paso
from airflow import DAG
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from airflow.operators.bash_operator import BashOperator

from random import randint
from datetime import datetime

def _choose_best_model(**kwargs):
    accuracies = kwargs['ti'].xcom_pull(task_ids=[
        'training_model_A',
        'training_model_B',
        'training_model_C'
    ])
    best_accuracy = max(accuracies)
    if (best_accuracy > 8):
        return 'accurate'
    return 'inaccurate'


def _training_model():
    return randint(1, 10)
  
#segundo paso
with DAG("DO_004_Modelo_Xcom2", start_date=datetime(2021, 1, 1), schedule_interval="@daily", catchup=False) as dag:
#tercer paso
    training_model_A = PythonOperator(
        task_id="training_model_A",
        python_callable=_training_model
        )

    training_model_B = PythonOperator(
        task_id="training_model_B",
        python_callable=_training_model
        )

    training_model_C = PythonOperator(
        task_id="training_model_C",
        python_callable=_training_model
        )

    choose_best_model = BranchPythonOperator(
        task_id="choose_best_model",
        provide_context=True,
        python_callable=_choose_best_model
        )

    accurate = BashOperator(
        task_id="accurate",
        bash_command="echo 'accurate'"
        )

    inaccurate = BashOperator(
        task_id="inaccurate",
        bash_command="echo 'inaccurate'"
        )

#cuarto paso
[training_model_A, training_model_B, training_model_C] >> choose_best_model >> [accurate, inaccurate]

