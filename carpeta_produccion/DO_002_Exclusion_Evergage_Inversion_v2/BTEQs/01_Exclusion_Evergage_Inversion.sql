/*********************************************************************
**********************************************************************
** DSCRPCN: SE GENERA EXCLUSION PARA EVERGAGE INVERSIONES		  **
**          			 						  **
** AUTOR  : MANUEL HENRIQUEZ				                    **
** EMPRESA: BCI                                 			  **
** FECHA  : 06/2021                                                 **
*********************************************************************/
/*********************************************************************
** TABLA DE ENTRADA :   MKT_EXPLORER_TB.INV_GST_APORT_RESCATE_BAM
                        MKT_CRM_ANALYTICS_TB.S_PERSONA

** TABLA DE SALIDA  : MKT_ANALYTICS_PER_TB.I_SF_EXCL_EVERGAGE_FFMM_TEMP

***********************************************************************
*********************************************************************/
.SET SESSION CHARSET 'UTF8';
SELECT DATE, TIME;

/**********************************************************************
** Verifica si existen las tablas, si existen las borra.
***********************************************************************/


SELECT DatabaseName, TableName FROM DBC.Tables WHERE DatabaseName = '{{ var.value.BD_TEMP_ANALYTICS }}' AND TableName = 'T_SF_EXCL_EVGE_FFMM_AUX_TEMP';
.IF ACTIVITYCOUNT > 0 THEN DROP TABLE {{ var.value.BD_TEMP_ANALYTICS }}.T_SF_EXCL_EVGE_FFMM_AUX_TEMP;

SELECT DatabaseName, TableName FROM DBC.Tables WHERE DatabaseName = '{{ var.value.BD_TEMP_ANALYTICS }}' AND TableName = 'T_SF_EXCL_EVERGAGE_FFMM_TEMP';
.IF ACTIVITYCOUNT > 0 THEN DROP TABLE {{ var.value.BD_TEMP_ANALYTICS }}.T_SF_EXCL_EVERGAGE_FFMM_TEMP;


/**********************************************************************
** Creacion de tablas.
***********************************************************************/

CREATE MULTISET TABLE {{ var.value.BD_TEMP_ANALYTICS }}.T_SF_EXCL_EVGE_FFMM_AUX_TEMP ,FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      Te_Cli_Rut INTEGER,
      Tf_Fecha DATE FORMAT 'YYYY-MM-DD',
      Tc_Tipo_Operacion VARCHAR(15) CHARACTER SET LATIN NOT CASESPECIFIC,
      Tc_Familia VARCHAR(15) CHARACTER SET LATIN NOT CASESPECIFIC)
PRIMARY INDEX ( Te_Cli_Rut );
.IF ERRORCODE <> 0 THEN QUIT 0001;

CREATE MULTISET TABLE {{ var.value.BD_TEMP_ANALYTICS }}.T_SF_EXCL_EVERGAGE_FFMM_TEMP ,FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      Te_Cli_Rut INTEGER,
      Te_Per_CIC VARCHAR(12) CHARACTER SET LATIN NOT CASESPECIFIC,
      Tc_Tipo_Operacion VARCHAR(15) CHARACTER SET LATIN NOT CASESPECIFIC,
      Tc_Familia VARCHAR(15) CHARACTER SET LATIN NOT CASESPECIFIC,
      Tf_Fecha_FFMM DATE FORMAT 'yyyy-mm-dd',
      Te_Flg_FFMM BYTEINT,
      Tf_Fecha_Proceso DATE FORMAT 'yyyy-mm-dd')
PRIMARY INDEX (Te_Cli_Rut,Te_Per_CIC );
.IF ERRORCODE <> 0 THEN QUIT 0002;

/**********************************************************************
** Inserción de datos.
***********************************************************************/
INSERT INTO {{ var.value.BD_TEMP_ANALYTICS }}.T_SF_EXCL_EVGE_FFMM_AUX_TEMP
SELECT
	Rut,
	Fecha,
	Tipo_Operacion,
	Familia
FROM MKT_EXPLORER_TB.INV_GST_APORT_RESCATE_BAM
WHERE Tipo_Operacion = 'Aporte'
AND Fecha BETWEEN CAST( '{{ds_nodash}}' AS DATE FORMAT 'YYYYMMDD') - 7
            AND CAST( '{{ds_nodash}}' AS DATE FORMAT 'YYYYMMDD') 
AND Rut < 50000000
QUALIFY ROW_NUMBER() OVER(PARTITION BY Rut ORDER  BY Fecha DESC) = 1;
.IF ERRORCODE <> 0 THEN QUIT 0003;

COLLECT STATS INDEX (Te_Cli_Rut)
	ON {{ var.value.BD_TEMP_ANALYTICS }}.T_SF_EXCL_EVGE_FFMM_AUX_TEMP;
.IF ERRORCODE <> 0 THEN QUIT 0004;

INSERT INTO {{ var.value.BD_TEMP_ANALYTICS }}.T_SF_EXCL_EVERGAGE_FFMM_TEMP
SELECT
	A.Te_Cli_Rut,
	TRIM(B.Sc_Per_CIC) (VARCHAR(12)) Ie_Per_CIC,
	A.Tc_Tipo_Operacion,
	A.Tc_Familia,
	A.Tf_Fecha,
      1 AS Ie_Flg_FFMM,
      CURRENT_TIMESTAMP AS It_Fecha_Proceso
FROM {{ var.value.BD_TEMP_ANALYTICS }}.T_SF_EXCL_EVGE_FFMM_AUX_TEMP A
LEFT JOIN MKT_CRM_ANALYTICS_TB.S_PERSONA B
ON A.Te_Cli_Rut = B.Se_Per_Rut
WHERE B.Se_Per_Cant_Cct > 0 OR B.Se_Per_Cant_Cpr > 0
AND B.Se_Per_Edad > 18;
.IF ERRORCODE <> 0 THEN QUIT 0005;

COLLECT STATS INDEX (Te_Cli_Rut,Te_Per_CIC)
	ON {{ var.value.BD_TEMP_ANALYTICS }}.T_SF_EXCL_EVERGAGE_FFMM_TEMP;
.IF ERRORCODE <> 0 THEN QUIT 0006;

-- En caso de reproceso, elimina los datos cargados en la fecha indicada
DELETE FROM {{ var.value.BD_PROD_ANALYTICS }}.I_SF_EXCL_EVERGAGE_FFMM_TEMP
WHERE If_Fecha_Proceso = CAST( '{{ds_nodash}}' AS DATE FORMAT 'YYYYMMDD');
.IF ERRORCODE <> 0 THEN QUIT 0007;

-- Inserta en tabla de respaldo
INSERT INTO {{ var.value.BD_PROD_ANALYTICS }}.I_SF_EXCL_EVERGAGE_FFMM_TEMP
SELECT
      Te_Cli_Rut as Ie_Cli_Rut,
      Te_Per_CIC AS Ie_Per_CIC,
      Tc_Tipo_Operacion AS Ic_Tipo_Operacion,
      Tc_Familia AS Ic_Familia,
      Tf_Fecha_FFMM AS If_Fecha_FFMM,
      Te_Flg_FFMM AS Ie_Flg_FFMM,
      Tf_Fecha_Proceso AS If_Fecha_Proceso
FROM {{ var.value.BD_TEMP_ANALYTICS }}.T_SF_EXCL_EVERGAGE_FFMM_TEMP;
.IF ERRORCODE <> 0 THEN QUIT 0008;

COLLECT STATS INDEX (Ie_Cli_Rut, If_Fecha_Proceso)
	ON {{ var.value.BD_PROD_ANALYTICS }}.I_SF_EXCL_EVERGAGE_FFMM_TEMP;
.IF ERRORCODE <> 0 THEN QUIT 0009;

---- FIN
SELECT DATE, TIME;
.QUIT 0;