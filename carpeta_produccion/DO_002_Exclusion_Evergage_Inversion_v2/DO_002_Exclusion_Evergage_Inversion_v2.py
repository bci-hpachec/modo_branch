# -*- coding: utf-8 -*-
from airflow import DAG, utils
from airflow.models import Variable
from datetime import datetime, timedelta, date, time
from airflow.operators.bcitools import BteqOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators import DummyOperator
from airflow.contrib.operators.sftp_operator import SFTPOperator
from airflow.operators.bash_operator import BashOperator
from airflow.contrib.hooks import SSHHook
import logging
import os
import pendulum
import glob
import pandas as pd
import giraffez
import uuid

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

default_args = {
    'owner': 'DO',
    'start_date': utils.dates.days_ago(3).astimezone(pendulum.timezone(Variable.get('TZ'))),
    #'email': ['lista_correops'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}
"""
Fin de configuracion basica del DAG
"""

teradata_credentials = Variable.get("td_credentials_prod", deserialize_json=True)

dag = DAG('DO_002_Exclusion_Evergage_Inversion_v2', default_args=default_args, schedule_interval="00 14 * * 1-5",tags=['DO','EVERGAGE','INVERSION'])
t0 = DummyOperator(task_id='Inicio_Proceso_14_00',dag=dag)

dag_tasks = [t0]

def define_bteq_op(bteq_file, bteq_name, bteq_params={}):
    """ Define operador BTEQ para ejecutar en contenedor"""
    return BteqOperator(
        bteq=os.path.join(queries_folder,os.path.basename(bteq_file)),
        task_id=bteq_name,
        conn_id='td_ttu',
        pool='teradata',
        depends_on_past=False,
        provide_context=True,
        xcom_push=True,
        xcom_all=True,
    dag=dag)

# Dummy Operator
dummy_fin = DummyOperator(
    task_id='Terminado',
    dag=dag
)

queries_folder = 'BTEQs'
ext_file = '.sql'
bteq_files = [f for f in glob.glob('%s/*%s' % (os.path.join(__location__, queries_folder), ext_file)) if
              f.endswith(ext_file)]

for bteq_file in sorted(bteq_files):
    try:
        query_task_id = os.path.basename(bteq_file).split(ext_file)[0]
    except:
        query_task_id = str(uuid.uuid4())
        logging.info("Archivo con el nombre malo : " + bteq_file)
        pass
    dag_tasks.append(define_bteq_op(bteq_file, query_task_id))

select_query = """
SELECT
    Te_Per_CIC AS Ie_Per_CIC,
    Tc_Tipo_Operacion AS Ic_Tipo_Operacion,
    Tc_Familia AS Ic_Familia,
    Tf_Fecha_FFMM AS If_Fecha_FFMM,
    Te_Flg_FFMM AS Ie_Flg_FFMM
FROM """+Variable.get('BD_TEMP_ANALYTICS')+""".T_SF_EXCL_EVERGAGE_FFMM_TEMP;
"""
"""
class query_to_csvOperator (PythonOperator): #CLASE QUE PERMITE USARLA COMO OPERADOR Y PASAR POR TEMPLATE DENTRO DEL DAG
    template_ext = ('.sql')

def query_tocsv(**kwargs):
    
    td_config = teradata_credentials
    
    today = date.today()
    today = today.strftime("%d_%m_%Y")

    filename="Batch_Exclusion_Evergage_Inv_"+today+".csv"
    ruta = "/files/tmp/Batch_Exclusion_Evergage_Inv/"
    archivo_return = ruta + filename
    with giraffez.BulkExport(
            select_query
            , **td_config) as export:
        output = pd.DataFrame.from_dict(export.to_dict())
    
    output = output[[
    'ie_per_cic',
    'ic_tipo_operacion',
    'ic_familia',
    'if_fecha_ffmm',
    'ie_flg_ffmm'
    ]]
    
    output.to_csv(archivo_return, sep=';', encoding='utf8', header=True, index=None)
    
    return archivo_return

Export_CSV_Teradata = query_to_csvOperator(
     task_id = '02_Export_CSV_Teradata'
    ,provide_context = True
    ,python_callable = query_tocsv
    ,dag=dag
)

#FTPOPERATOR
# ftp_aprimo
today = date.today()
today = today.strftime("%d_%m_%Y")
ruta = "/files/tmp/Batch_Exclusion_Evergage_Inv/Batch_Exclusion_Evergage_Inv_"+today+".csv"
archivo = "Batch_Exclusion_Evergage_Inv_" + today + ".csv"

hook = SSHHook(ssh_conn_id='sftp_ssh_salesforce')
hook.no_host_key_check = True
Ejecuta_ftp_operator = SFTPOperator(
        task_id = '03_ssh_ftp_salesforce_task',
        ssh_hook = hook,
        local_filepath = ruta,
        remote_filepath = '/DataSalesforce/mktcloud/import/'+archivo,
        operation = 'put',
        dag=dag
)

Eliminar_Archivos = BashOperator(
    task_id='04_Eliminar_Archivos',
    provide_context=True,
    wait_for_downstream=True,
    bash_command='./04_Eliminar_Archivos.sh',
    params={'result_path': '/files/tmp/Batch_Exclusion_Evergage_Inv/'
            },
    dag=dag
)
"""
# Definiendo dependencias, se asume secuencialidad.
for seq_pair in zip(dag_tasks, dag_tasks[1:]):
    seq_pair[0] >> seq_pair[1]

#seq_pair[1] >> Export_CSV_Teradata >> Ejecuta_ftp_operator >> Eliminar_Archivos >> dummy_fin
seq_pair[1] >> dummy_fin

